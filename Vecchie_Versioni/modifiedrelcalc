(* ========================================================================= *)
(* Calcolo relazionale sui domini.                                           *)
(* ========================================================================= *)

(* ------------------------------------------------------------------------- *)
(* Termini.                                                                  *)
(* `Var(i,d)` è la variabile di indice `i` e dominio `d`.                    *)
(* ------------------------------------------------------------------------- *)

let tm_INDUCT,tm_RECUR = define_type
  "tm = Var (num # num)
      | Const num";;

(* ------------------------------------------------------------------------- *)
(* Variabili dei termini.                                                    *)
(* ------------------------------------------------------------------------- *)

let TMVARS = new_recursive_definition tm_RECUR
  `(!a. TMVARS (Var a) = {a}) /\
   (!k. TMVARS (Const k) = {})`;;

let TMVARS_LIST = new_recursive_definition list_RECURSION
  `TMVARS_LIST [] = {} /\
   (!x xs. TMVARS_LIST (CONS x xs) = TMVARS x UNION TMVARS_LIST xs)`;;

(*
g `!a xs. a IN TMVARS_LIST xs <=> EX (\x. a IN TMVARS x) xs`;;
e (GEN_TAC);;
e (MATCH_MP_TAC list_INDUCT);;
e (REWRITE_TAC[TMVARS_LIST; EX; NOT_IN_EMPTY]);;
e (INTRO_TAC "![y] [ys]; ind");;
e (ASM_REWRITE_TAC[IN_UNION]);;
let IN_TMVARS_LIST = top_thm();;
*)

let IN_TMVARS_LIST = prove
 (`!a xs. a IN TMVARS_LIST xs <=> EX (\x. a IN TMVARS x) xs`,
  GEN_TAC THEN MATCH_MP_TAC list_INDUCT THEN
  REWRITE_TAC[TMVARS_LIST; EX; NOT_IN_EMPTY] THEN
  INTRO_TAC "![y] [ys]; ind" THEN
  ASM_REWRITE_TAC[IN_UNION]);;

(* ------------------------------------------------------------------------- *)
(* Sostituzione per i termini.                                               *)
(* ------------------------------------------------------------------------- *)

(* ------------------------------------------------------------------------- *)
(* `TMSUBST f x` sostituisce dentro il termine x usando la mappa             *)
(* `f:num -> tm` da pensare come assegnazione di sostituzione a variabile    *)
(* numero `n` associo il termine `f n`.                                      *)
(* ------------------------------------------------------------------------- *)

let TMSUBST = new_recursive_definition tm_RECUR
  `(!f a. TMSUBST f (Var a) = f a) /\
   (!f k. TMSUBST f (Const k) = Const k)`;;

g `(!x. TMSUBST Var x = x)`;;
e (MATCH_MP_TAC tm_INDUCT);;
e (REWRITE_TAC[TMSUBST]);;
let TMSUBST_VAR = top_thm();;

g `(!f x g. TMSUBST f (TMSUBST g x) = TMSUBST (TMSUBST f o g) x)`;;
e (GEN_TAC THEN MATCH_MP_TAC tm_INDUCT);;
e (REWRITE_TAC[TMSUBST; o_THM]);;
let TMSUBST_TMSUBST = top_thm();;

g `!f x. (!a. a IN TMVARS x ==> f a = Var a) ==> TMSUBST f x = x`;;
e (GEN_TAC THEN MATCH_MP_TAC tm_INDUCT);;
e (REWRITE_TAC [TMVARS; TMSUBST; FORALL_IN_INSERT; NOT_IN_EMPTY]);;
let TMSUBST_ID = top_thm();;

g `(!f x. TMVARS x = {} ==> TMSUBST f x = x)`;;
e (GEN_TAC THEN MATCH_MP_TAC tm_INDUCT);;
e (REWRITE_TAC[TMVARS; TMSUBST; NOT_INSERT_EMPTY]);;
let CLOSED_IMP_TMSUBST_ID = top_thm();;

(* ------------------------------------------------------------------------- *)
(* Formule.                                                                  *)
(* ------------------------------------------------------------------------- *)

parse_as_infix("&&",(16,"right"));;
parse_as_infix("||",(15,"right"));;
parse_as_infix("-->",(14,"right"));;
parse_as_infix("<->",(13,"right"));;
parse_as_prefix "Not";;

let form_INDUCT,form_RECUR = define_type
  "form = Atom num (tm list)
        | Ge tm tm
        | False
        | --> form form
        | Forall num form";;

(* ------------------------------------------------------------------------- *)
(* Variabili dei termini.                                                    *)
(* ------------------------------------------------------------------------- *)

let form_not    = new_definition `Not p      = p --> False`;;
let form_or     = new_definition `p || q     = Not p --> q`;;
let form_and    = new_definition `p && q     = Not (p --> Not q)`;;
let form_iff    = new_definition `p <-> q    = (p --> q) && (q --> p)`;;
let form_exists = new_definition `Exists d p = Not (Forall d (Not p))`;;

(* ------------------------------------------------------------------------- *)
(* Variabili libere.                                                         *)
(* ------------------------------------------------------------------------- *)

let IN_FVARS_RULES,IN_FVARS_INDUCT,IN_FVARS_CASES = new_inductive_set
  `(!a k xs. a IN TMVARS_LIST xs ==> a IN FVARS (Atom k xs)) /\
   (!a x1 x2. a IN TMVARS x1 \/ a IN TMVARS x2 ==> a IN FVARS (Ge x1 x2)) /\
   (!a p q. a IN FVARS p ==> a IN FVARS (p --> q)) /\
   (!a p q. a IN FVARS q ==> a IN FVARS (p --> q)) /\
   (!d i p. (SUC i,d) IN FVARS p ==> (i,d) IN FVARS (Forall d p)) /\
   (!d i e p. (i,d) IN FVARS p /\ ~(d = e) ==> (i,d) IN FVARS (Forall e p))`;;

(*
let FVARS = prove
  (`(!k xs. FVARS (Atom k xs) = TMVARS_LIST xs) /\
   FVARS False = {} /\
   (!p q. FVARS (p --> q) = FVARS p UNION FVARS q) /\
   (!e p. FVARS (Forall e p) = {i,e | i | SUC i,e IN FVARS p} UNION
                               {i,d | ~(d = e) /\ i,d IN FVARS p})`,
  REPEAT STRIP_TAC THEN GEN_REWRITE_TAC I [EXTENSION] THEN GEN_TAC THEN
  GEN_REWRITE_TAC LAND_CONV [IN_FVARS_CASES] THEN
  REWRITE_TAC[distinctness "form"; injectivity "form";
              IN_TMVARS_LIST; NOT_IN_EMPTY] THENL
  [MESON_TAC[EX_MEM]; SET_TAC[]; SET_TAC[]]);;
*)

g `(!k xs. FVARS (Atom k xs) = TMVARS_LIST xs) /\
   (!x1 x2. FVARS (Ge x1 x2) = TMVARS x1 UNION TMVARS x2) /\
   FVARS False = {} /\
   (!p q. FVARS (p --> q) = FVARS p UNION FVARS q) /\
   (!e p. FVARS (Forall e p) = {i,e | i | SUC i,e IN FVARS p} UNION
                               {i,d | ~(d = e) /\ i,d IN FVARS p})`;;
e (REPEAT STRIP_TAC);;
e (GEN_REWRITE_TAC I [EXTENSION]);;
e (GEN_TAC);;
e (GEN_REWRITE_TAC LAND_CONV [IN_FVARS_CASES]);;
e (REWRITE_TAC [distinctness "form"; injectivity "form";
                IN_TMVARS_LIST; NOT_IN_EMPTY]);;
e (MESON_TAC [EX_MEM]);;
e (GEN_REWRITE_TAC I [EXTENSION]);;
e (GEN_TAC);;
e (GEN_REWRITE_TAC LAND_CONV [IN_FVARS_CASES]);;
e (REWRITE_TAC [distinctness "form"; injectivity "form"; TMVARS; IN_UNION]);;
e (MESON_TAC[]);;
e (GEN_REWRITE_TAC I [EXTENSION]);;
e (GEN_TAC);;
e (GEN_REWRITE_TAC LAND_CONV [IN_FVARS_CASES]);;
e (REWRITE_TAC[distinctness "form"; injectivity "form"; NOT_IN_EMPTY]);;
e (GEN_REWRITE_TAC I [EXTENSION]);;
e (GEN_TAC);;
e (GEN_REWRITE_TAC LAND_CONV [IN_FVARS_CASES]);;
e (REWRITE_TAC [distinctness "form"; injectivity "form"]);;
e (SET_TAC []);;
e (GEN_REWRITE_TAC I [EXTENSION]);;
e (GEN_TAC);;
e (GEN_REWRITE_TAC LAND_CONV [IN_FVARS_CASES]);;
e (REWRITE_TAC [distinctness "form"; injectivity "form"]);;
e (SET_TAC[]);;
let FVARS = top_thm();;

(* ------------------------------------------------------------------------- *)
(* Sostituzione.                                                             *)
(* ------------------------------------------------------------------------- *)

let BUMP = new_definition
  `BUMP e = TMSUBST (\(i,d). Var((if d = e then SUC i else i),d))`;;

let SLIDE = new_definition
  `SLIDE e f (i,d) = if d = e
                     then if i = 0 
                          then Var(0,d)
                          else BUMP e (f(PRE i,d))
                     else f(i,d)`;;

let SUBST = define
  `(!f k xs. SUBST f (Atom k xs) = Atom k (MAP (TMSUBST f) xs)) /\
   (!f x1 x2. SUBST f (Ge x1 x2) = Ge (TMSUBST f x1) (TMSUBST f x2)) /\
   (!f. SUBST f False = False) /\
   (!f p q. SUBST f (p --> q) = SUBST f p --> SUBST f q) /\
   (!f d p. SUBST f (Forall d p) = Forall d (SUBST (SLIDE d f) p))`;;

(* ek associa ad ogni nome di costante (un num) il nome del dominio (un num)   *)
(* ed associa ad ogni nome di dominio (un num) il dominio (un insieme num->bool*)
(* DOMAIN associa ad ogni termine il nome del dominio a cui appartiene.        *)

let DOMAIN = new_definition
  `DOMAIN ek d = {Const k | k | ek k = d}`;;

let KIND = define
  `(!ek i d. KIND ek (Var (i,d)) = d) /\
   (!ek k. KIND ek (Const k) = ek k)`;;

g `!ek f x. (!j e. KIND ek (f(j,e)) = e) ==> KIND ek (TMSUBST f x) = KIND ek x`;;
e (GEN_TAC THEN GEN_TAC THEN MATCH_MP_TAC tm_INDUCT);;
e (REWRITE_TAC[TMSUBST; FORALL_PAIR_THM; KIND]);;
e (MESON_TAC[]);;
let KIND_TMSUBST = top_thm();;

g `!ek e x. KIND ek (BUMP e x) = KIND ek x`;;
e (REPEAT GEN_TAC THEN REWRITE_TAC[BUMP]);;
e (MATCH_MP_TAC KIND_TMSUBST);;
e (REWRITE_TAC[KIND]);;
let KIND_BUMP = top_thm();;

g `!ek f i d. (!j e. KIND ek (f(j,e)) = e) ==> KIND ek (SLIDE e f (i,d)) = d`;;
e (REPEAT GEN_TAC THEN DISCH_TAC);;
e (REWRITE_TAC[SLIDE]);;
e COND_CASES_TAC;;
e (POP_ASSUM SUBST1_TAC);;
e COND_CASES_TAC;;
e (POP_ASSUM SUBST1_TAC);;
e (REWRITE_TAC[KIND]);;
e (ASM_REWRITE_TAC[KIND_BUMP]);;
e (ASM_REWRITE_TAC[]);;
let KIND_SLIDE = top_thm();;

g `!ek a f g.
      (!i d. KIND ek (f(i,d)) = d) /\ (!i d. KIND ek (g(i,d)) = d)
      ==> SUBST f (SUBST g a) = SUBST (TMSUBST f o g) a`;;
e GEN_TAC;;
e (MATCH_MP_TAC form_INDUCT);;
e (REWRITE_TAC[SUBST; injectivity "form"]);;
e (REWRITE_TAC[GSYM MAP_o]);;
e (REWRITE_TAC[o_DEF; TMSUBST_TMSUBST; ETA_AX]);;
e (CONJ_TAC THEN REPEAT GEN_TAC THEN STRIP_TAC);;
e (ASM_SIMP_TAC[]);;
e (REPEAT GEN_TAC THEN STRIP_TAC);;
e (ASM_SIMP_TAC[KIND_SLIDE]);;          (*            DA CAPIRE                *)
e (AP_THM_TAC THEN AP_TERM_TAC);;                    (*semplifico i due termini*)
e (REWRITE_TAC[FUN_EQ_THM]);;                 (*applico !x da entrambe le parte
                                                         e semplifico un lambda*)
e (REWRITE_TAC[FORALL_PAIR_THM]);;      (*anziché x metto una coppia di termini*)
e (INTRO_TAC "![i] [d]");;                             (*chiamo p1 p2 con i e d*)
e (REWRITE_TAC[SLIDE]);;                 (*riscrivo SLIDE e viene fuori il COND*)
e COND_CASES_TAC;;                (*I analisi per casi: d = a0 oppure ~(d = a0)*)
e (POP_ASSUM (SUBST1_TAC o GSYM));;          (*riscrivo al posto di a0,d: d->a0*)
e (REWRITE_TAC[]);;               (*riscrivo così da semplificare il II termine*)
e COND_CASES_TAC;;                           (*ho il II condizionale nidificato*)
e (POP_ASSUM SUBST1_TAC);;     (*il caso di i=0 è abbastanza semplice risolvere*)
e (REWRITE_TAC[TMSUBST; SLIDE]);;        (*0->i, riscrivo e semplifico lo slide*)
e (REWRITE_TAC[BUMP]);;  (*ATTENZIONE:sono del caso di ~(i = 0) dove userò BUMP*)
e (CLAIM_TAC "rmk"
     `!j e. KIND ek ((\ (i,d'). Var ((if d' = d then SUC i else i),d'))
                    (j,e)) = e`);;     (*ho bisogno di un "lemma" a metà strada*)
e (REWRITE_TAC[KIND]);;             (*risolvo rmk e ora è nelle assunzioni*)
e (ASM_SIMP_TAC[TMSUBST_TMSUBST; KIND_SLIDE]);; (*faccio composizione dei termini
                                               per poi semplificarli come priam*)
e (AP_THM_TAC THEN AP_TERM_TAC);;  (*semplifico i termini*)
e (REWRITE_TAC[FUN_EQ_THM]);;    (*come prima*)
e (REWRITE_TAC[FORALL_PAIR_THM]);;
e (INTRO_TAC "![j] [e]");;
e (REWRITE_TAC[o_THM; TMSUBST; SLIDE]);; (*PROMEMORIA!!!*)
(*o_THM scioglie lambda del  primo termine; TMSUBST si toglie
nel primo termine e SLIDE riapre un condizionale
*)  
e COND_CASES_TAC;;               (*faccio analisi per casi di nuovo sullo SLIDE*)
e (POP_ASSUM SUBST1_TAC);;    (*sostituisco solo la d->e, non sembra modif BUMP*)
e (REWRITE_TAC[NOT_SUC; PRE; BUMP]);; (*risolvo il caso I: e = d*) (*PROBL*)
e (REWRITE_TAC[]);;  (*da qui che è da rivedere!!!!*)
e (MATCH_MP_TAC EQ_TRANS);;
e (EXISTS_TAC `TMSUBST Var (f (j:num,e:num))`);;
e CONJ_TAC;;
e (REWRITE_TAC[TMSUBST_VAR]);;
e (CLAIM_TAC "k" `KIND ek (f(j:num,e)) = e`);;
e (ASM_REWRITE_TAC[]);;
e (POP_ASSUM MP_TAC);;
e (STRUCT_CASES_TAC (SPEC `f(j:num,e:num):tm` (cases "tm")) THEN
   REWRITE_TAC[TMSUBST]);;
e (STRUCT_CASES_TAC (ISPEC `a:num#num` PAIR_SURJECTIVE));;
e (REWRITE_TAC[KIND; injectivity "tm"; PAIR_EQ]);; (*QUI la def bumb1 non va!*)
e (DISCH_THEN SUBST_ALL_TAC);;
e (ASM_REWRITE_TAC[]);;     (*qui finisce la parte da riguardare*)
e (REWRITE_TAC[]);; (*!!!*)
e (CLAIM_TAC "k" `KIND ek (g(i:num,d)) = d`);;
e (ASM_REWRITE_TAC[]);;
e (POP_ASSUM MP_TAC);;
e (STRUCT_CASES_TAC (SPEC `g(i:num,d:num):tm` (cases "tm")) THEN
   REWRITE_TAC[TMSUBST]);;
e (STRUCT_CASES_TAC (ISPEC `a:num#num` PAIR_SURJECTIVE));;
e (REWRITE_TAC[KIND; injectivity "tm"; PAIR_EQ]);;
e (DISCH_THEN SUBST_ALL_TAC);;
e (ASM_REWRITE_TAC[SLIDE]);;
let SUBST_SUBST = top_thm();;

g `!p f. (!a. a IN FVARS p ==> f a = Var a)
           ==> SUBST f p = p `;;
e (MATCH_MP_TAC form_INDUCT);;
e (REWRITE_TAC[FVARS; SUBST; injectivity "form"]);; (*Semplifico, elimino False*)
e CONJ_TAC;;
e (MATCH_MP_TAC list_INDUCT);;
e (REWRITE_TAC [MAP; IN_TMVARS_LIST; EX]);;
e (SIMP_TAC [TMSUBST_ID]);;  (*Elimino il caso di Atomo*)
e (SIMP_TAC[TMSUBST; TMVARS; IN_UNION; IN_SING; TMSUBST_ID; FORALL_IN_UNION]);;
  (*Elimino Ge e -->*)
e (REWRITE_TAC[FORALL_IN_GSPEC]);;
e (REPEAT STRIP_TAC);;
e (FIRST_X_ASSUM MATCH_MP_TAC);;
e (REWRITE_TAC [FORALL_PAIR_THM; SLIDE]);;
e (REPEAT STRIP_TAC);;
e (COND_CASES_TAC);;   (*analisi COND più esterno*)
e (POP_ASSUM SUBST_VAR_TAC);;
e (COND_CASES_TAC);; (*analisi COND più interno*)
e (ASM_REWRITE_TAC[]);; (*ho risolto il if=V del COND più interno*)
e (REWRITE_TAC[BUMP]);;   (*pdp per nuovo BUMP*)
e (ASM_SIMP_TAC[ARITH_RULE `~(p1 = 0) ==> SUC (PRE p1) = p1`]);;
e (REWRITE_TAC [TMSUBST]);;
e (ASM_SIMP_TAC[ARITH_RULE `~(p1 = 0) ==> SUC (PRE p1) = p1`]);;
e (FIRST_X_ASSUM MATCH_MP_TAC);;
e (ASM_REWRITE_TAC[]);;
let SUBST_ID = top_thm();;

g `!f p. FVARS p = {} ==> SUBST f p = p`;;
e (REPEAT STRIP_TAC);;
e (MATCH_MP_TAC SUBST_ID);;
e (ASM_SIMP_TAC[NOT_IN_EMPTY]);;
let CLOSED_IMP_SUBST_ID = top_thm();;

(* ------------------------------------------------------------------------- *)
(* Interpretazione dei termini.                                              *)
(* ------------------------------------------------------------------------- *)

let TMINTERP = define
  `(!ev a. TMINTERP ev (Var(a)) = ev a) /\
   (!ev k. TMINTERP ev (Const k) = k) `;;

let TMLIST_INTERP = define
  `(!ev. TMLIST_INTERP ev [] = []) /\
   (!ev x xs. TMLIST_INTERP ev (CONS x xs) = 
                 CONS (TMINTERP ev x) (TMLIST_INTERP ev xs))`;;

g `!ev f x. TMINTERP ev (TMSUBST f x) = 
            TMINTERP (TMINTERP ev o f) x`;;
e (GEN_TAC THEN GEN_TAC);;
e (MATCH_MP_TAC tm_INDUCT);;
e (REWRITE_TAC [TMSUBST; injectivity "tm"; TMINTERP; o_THM]);;
let TMINTERP_TMSUBST = top_thm();;

(* ------------------------------------------------------------------------- *)
(* Interpretazione delle formule                                             *)
(* ------------------------------------------------------------------------- *)

let INTERP = define
  `(!ed ep ev k xs. INTERP ed ep ev (Atom k xs)
                <=> ep k (MAP (TMINTERP ev) xs)) /\
   (!ed ep ev x1 x2. INTERP ed ep ev (Ge x1 x2)
                <=>  TMINTERP ev x1 <= TMINTERP ev x2) /\
   (!ed ep ev. INTERP ed ep ev False <=> F) /\
   (!ed ep ev p q. INTERP ed ep ev (p --> q) <=>
                  (INTERP ed ep ev (p)) ==> (INTERP ed ep ev (q))) /\ 
   (!ed ep ev d p. INTERP ed ep ev (Forall d p) <=>
                   (!x. x IN ed d
        	        ==> INTERP ed ep
                                   (\ (i,e). if e = d
 				             then if i = 0
 					          then x
						  else ev (PRE i,d)
 					     else ev (i,e))
                                   p))`;;


(* 
(map dest_var o frees)
  `(!ed ep ev k xs. INTERP ed ep ev (Atom k xs)
                <=> ep k (TM_LIST_INTERP ev xs)) /\
   (!ed ep ev. INTERP ed ep ev False <=> F) /\
   (!ed ep ev p q. INTERP ed ep ev (p --> q) <=>
                   (INTERP ed ep ev (p)) ==> (INTERP ed ep ev (q))) /\ 
   (!ed ep ev d p. INTERP ed ep ev (Forall d p) <=>
                   (!x. x IN ed d
        	        ==> INTERP ed ep
                                   (\ (i,e). if e = d
 				             then if i = 0
 					          then x
						  else ev (PRE i,d)
					     else ev (i,e))
                                   p))`;;
*)
;;



g `!ed ep p ev f. INTERP ed ep ev (SUBST f p) =
           INTERP ed ep (TMINTERP ev o f) p`;;
e (GEN_TAC THEN GEN_TAC);;
e (MATCH_MP_TAC form_INDUCT);;
e (REWRITE_TAC [SUBST; injectivity "form"; INTERP]);; (*False*)
e CONJ_TAC;;
e (INTRO_TAC "!ev [k] [xs]");;
e (GEN_TAC);;
e (AP_TERM_TAC);;
e (REWRITE_TAC[GSYM MAP_o]);;
e (AP_THM_TAC THEN AP_TERM_TAC);;
e (REWRITE_TAC [FUN_EQ_THM]);;
e (REWRITE_TAC[o_THM]);;
e (REWRITE_TAC[TMINTERP_TMSUBST]);;   (*Atom*)
e CONJ_TAC;;
e (REWRITE_TAC[TMINTERP; TMINTERP_TMSUBST]);; (*Eg*)
e CONJ_TAC;;
e (REPEAT GEN_TAC THEN STRIP_TAC);;
e (ASM_SIMP_TAC[]);;                  (*-->*)


e (INTRO_TAC "! [d] [p]");;   (*da sistemare*)
e (REPEAT STRIP_TAC);;
e (ASM_REWRITE_TAC[]);;
e (SUBGOAL_THEN
`!x. x IN ed d ==>
          (TMINTERP
           (\ (i,e). if e = d
                    then if i = 0 then x else ev (PRE i,d)
                    else ev (i,e)) o
           SLIDE d f)
	   =
          (\ (i,e). if e = d
                   then if i = 0 then x else (TMINTERP ev o f) (PRE i,d)
                   else (TMINTERP ev o f) (i,e))`
  (fun th -> MESON_TAC[th]));;
e (INTRO_TAC "!x; x");;
e (REWRITE_TAC[FUN_EQ_THM; FORALL_PAIR_THM]);;
e (INTRO_TAC "![j] [b]");;
e (REWRITE_TAC[o_THM]);;

e (COND_CASES_TAC);;
e (POP_ASSUM SUBST1_TAC);;
e (COND_CASES_TAC);;
e (POP_ASSUM SUBST1_TAC);;
e (REWRITE_TAC [SLIDE; TMINTERP]);;

e (REWRITE_TAC[SLIDE]);;
e (ASM_SIMP_TAC[]);;
e (REWRITE_TAC[BUMP]);;
(*PROVA A SENTIMENTO    QUALCOSA DI POSITIVO IN QUELLO CHE HO SCRITTO C'È!!!*)
e (STRUCT_CASES_TAC (SPEC `f(PRE j:num,d:num):tm` (cases "tm")));;
e (REWRITE_TAC[TMINTERP; TMSUBST]);;
e (STRUCT_CASES_TAC (ISPEC `a:num#num` PAIR_SURJECTIVE));;
e (ASM_SIMP_TAC[TMINTERP]);;
e (COND_CASES_TAC);;
e (POP_ASSUM SUBST1_TAC);;
e (REWRITE_TAC[NOT_SUC; PRE]);;
e (ASM_SIMP_TAC[]);;
e (SIMP_TAC[TMINTERP; TMSUBST]);;
(*L'HO RIPASSATO TUTTO UNA VOLTA???*)
e (STRUCT_CASES_TAC (SPEC `f(j:num,b:num):tm` (cases "tm")));;

e(REWRITE_TAC[SLIDE]);;
e (ASM_SIMP_TAC[]);;
e (REWRITE_TAC[TMINTERP]);;

e (STRUCT_CASES_TAC (SPEC `f(j:num,b:num):tm` (cases "tm")));;
e (REWRITE_TAC[TMINTERP]);;
e (STRUCT_CASES_TAC (ISPEC `a:num#num` PAIR_SURJECTIVE));;
e (REWRITE_TAC[]);;
e (COND_CASES_TAC);;
e (POP_ASSUM SUBST1_TAC);;
e (ASM_SIMP_TAC[]);;

(*e EQ_TAC;;
e (INTRO_TAC "hp; !x; x");;*)

let INTERP_SUBST = top_thm();;
 

(*QUERY*)

let query_tybij = new_type_definition "query" ("mk_query","dest_query")
 (prove
    (`?(xs,p). TMVARS_LIST xs = FVARS p`,
     REWRITE_TAC[EXISTS_PAIRED_THM] THEN
     EXISTS_TAC `[]:tm list` THEN
     EXISTS_TAC `False` THEN
     REWRITE_TAC[TMVARS_LIST; FVARS]));;

let query_form = new_definition
  `query_form q = SND (dest_query q)`;;

let query_tms = new_definition
  `query_tms q = FST (dest_query q)`;;

let database = define_type
  "database = Database (num#num list -> bool)";;

let database_rels = define
  `database_rels (Database db) = db`;;   (*una relazioni singola è n#n->bool?*)

let EP_OF_DB = define
  `EP_OF_DB (Database db) k cs <=> db(k,cs)`;;   (*???*)

let DBINTERP = new_definition
  `DBINTERP ed db ev p <=> INTERP ed (EP_OF_DB db) ev p`;; (*???*)

let query_tpls = new_definition
  `query_tpls ed db q =
     {MAP (TMINTERP ev) (query_tms q) | ev |
      DBINTERP ed db ev (query_form q)}`;;       (*???*)

let run_query = define
  `run_query ed q k db =
     Database ({k,cs | cs IN query_tpls ed db q} UNION
               {j,cs | j,cs IN database_rels /\ ~(j = k)} db)`;;


(*
NNF_CONV `(!x. P x) ==> Q`;;
(NNF_CONV THENC PRENEX_CONV) `(!x. P x) ==> Q`;;

MESON [] `((!x. P x) ==> Q) <=> (?x. P x ==> Q)`;;
MESON [] `(P ==> (!x. Q x)) <=> (!x. P ==> Q x)`;;
MESON [] `(!x. F) <=> F`;;
*)

let FMPRENEX_IMP' = define
  `(!e p q. FMPRENEX_IMP' (Forall e p) q = Exists e (FMPRENEX_IMP' p q)) /\
   (!e p q. FMPRENEX_IMP' p (Forall e q) = Forall e (FMPRENEX_IMP' p q)) /\
   (!q. FMPRENEX_IMP' False q = False --> q)`;;

FMPRENEX_IMP' p q =
     match p with
       Forall e r -> Exists e (FMPRENEX_IMP' r q))
     | _ -> match q with
              Forall e r -> Forall e (FMPRENEX_IMP' p r)
            | _ -> p --> q`;;

(*
let FMPRENEX_IMP' = define
  `FMPRENEX_IMP' p q =
     match p with
       Forall e r -> Exists e (FMPRENEX_IMP' r q))
     | _ -> match q with
              Forall e r -> Forall e (FMPRENEX_IMP' p r)
            | _ -> p --> q`;;
*)

let FMPRENEX' = new_recursive_definition form_RECUR
  `(!k xs. FMPRENEX' (Atom k xs) = Atom k xs) /\
   FMPRENEX False = False /\
   (!p q. FMPRENEX' (p --> q) =
          FMPRENEX_IMP' (FMPRENEX' p) (FMPRENEX' q)) /\
   (!e p. FMPRENEX' (Forall e p) = Forall e (FMPRENEX' p))`;;

let FMPRENEX_IMP = define
  `FMPRENEX_IMP p =
     match p with
       (Forall e p) --> q -> Exists e (FMPRENEX_IMP (p --> q))
     | p --> (Forall e q) -> Forall e (FMPRENEX_IMP (p --> q))
     | _ -> p`;;

let FMPRENEX = new_recursive_definition form_RECUR
  `(!k xs. FMPRENEX (Atom k xs) = Atom k xs) /\
   FMPRENEX False = False /\
   (!p q. FMPRENEX (p --> q) =
          FMPRENEX_IMP (FMPRENEX p --> FMPRENEX q)) /\
   (!e p. FMPRENEX (Forall e p) = Forall e (FMPRENEX p))`;;

g `!ed ep ev p q.
     IS_PRENEX p /\ IS_PRENEX q
     ==> (INTERP ed ep ev (FMPRENEX_IMP p) <=> INTERP ed ep ev (p))`;;

g `!ed ep ev.
     !p. IS_PRENEX p
         ==> !q. IS_PRENEX q
                 ==> (INTERP ed ep ev (FMPRENEX_IMP p) <=>
                      INTERP ed ep ev (p))`;;

g `!ed ep ev p.INTERP ed ep ev (FMPRENEX_IMP p ) <=> INTERP ed ep ev (p)`;;
e (GEN_TAC THEN GEN_TAC THEN GEN_TAC);;
e (MATCH_MP_TAC form_INDUCT);;
e (CONJ_TAC);;
e (REWRITE_TAC[FMPRENEX_IMP]);;
e (CONJ_TAC);;
e (REWRITE_TAC[FMPRENEX_IMP]);;
e (CONJ_TAC);;
e (INTRO_TAC "![p] [q]; php qhp");;
e (ONCE_REWRITE_TAC[FMPRENEX_IMP]);;
e (REWRITE_TAC[INTERP]);;

MATCH_CONV 
`match p --> q with
    Forall e p --> q -> Exists e (FMPRENEX_IMP (p --> q))
  | p --> Forall e q -> Forall e (FMPRENEX_IMP (p --> q))
  | _ -> p --> q`;;

ONCE_DEPTH_CONV MATCH_CONV 
`INTERP ed ep ev
 (match p --> q with
    Forall e p --> q -> Exists e (FMPRENEX_IMP (p --> q))
  | p --> Forall e q -> Forall e (FMPRENEX_IMP (p --> q))
  | _ -> p --> q) <=>
 INTERP ed ep ev p ==> INTERP ed ep ev q`;;

e (CONV_TAC (ONCE_DEPTH_CONV MATCH_CONV));;
e COND_CASES_TAC;;
e (POP_ASSUM (DESTRUCT_TAC "@e r. r"));;
e (POP_ASSUM SUBST_VAR_TAC);;
e (REWRITE_TAC[]);;

e (STRUCT_CASES_TAC (SPEC `p:form` (cases "form")));;
e (STRUCT_CASES_TAC (SPEC `q:form` (cases "form")));;

e (STRIP_TAC);;
e (REWRITE_TAC[GSYM INTERP]);;
e (REWRITE_TAC[INTERP; FMPRENEX_IMP] THEN SIMP_TAC[]);;
e (REWRITE_TAC[INTERP; FMPRENEX_IMP] THEN SIMP_TAC[]);;
e (ASM_REWRITE_TAC[]);;
e (REWRITE_TAC[GSYM INTERP]);;
e (REWRITE_TAC[FMPRENEX_IMP]);;  (*VA Ad aprire la formula ad infinitum?*)


g `!ed ep ev p. INTERP ed ep ev (p) = INTERP ed ep ev (FMPRENEX p)`;;
e (GEN_TAC THEN GEN_TAC THEN GEN_TAC);;
e (MATCH_MP_TAC form_INDUCT);;
e (CONJ_TAC);;
e (INTRO_TAC "! [k] [xs]");;
e (REWRITE_TAC[INTERP; FMPRENEX]);;
e (CONJ_TAC);;
e (REWRITE_TAC[FMPRENEX]);;
e (CONJ_TAC);;
e (INTRO_TAC "! [p] [q]");;
e (REPEAT STRIP_TAC);;
e (REWRITE_TAC[INTERP; FMPRENEX]);;   (*poi devo dimostrare quello sopra*)



`query_tpls ed db q = query_tpls ed db (FMPRENEX q)`

`run_query ed q k db = run_query ed (FMPRENEX q) k db`;;


(*Calcolo congiuntivo in forma normale: applico due regole a una formula p:
1. sostituzione di variabile
2. fusione degli esistenziali: ?x1...xn.p /\ ?y1...yn.q <=>
                               ?x1...xn,y1...yn(p /\ q)
se x1-n e y1-n sono disgiunti, nessuno di x1-n occorre libero o vincolata in
 q e nessun y1-n occorre  libero o vincolato in p                         *)
