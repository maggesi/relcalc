(* ========================================================================= *)
(* Domain reletional calculus.                                               *)
(* ========================================================================= *)

(* ------------------------------------------------------------------------- *)
(* Terms.                                                                    *)
(* `Var(i,d)` is the variable with index `i` and domain `d`                  *)
(* ------------------------------------------------------------------------- *)

let tm_INDUCT,tm_RECUR = define_type
  "tm = Var (num # num)
      | Const num";;

(* ------------------------------------------------------------------------- *)
(* Term variables.                                                           *)
(* ------------------------------------------------------------------------- *)

let TMVARS = new_recursive_definition tm_RECUR
  `(!a. TMVARS (Var a) = {a}) /\
   (!k. TMVARS (Const k) = {})`;;

let TMVARS_LIST = new_recursive_definition list_RECURSION
  `TMVARS_LIST [] = {} /\
   (!x xs. TMVARS_LIST (CONS x xs) = TMVARS x UNION TMVARS_LIST xs)`;;

(*
g `!a xs. a IN TMVARS_LIST xs <=> EX (\x. a IN TMVARS x) xs`;;
e (GEN_TAC);;
e (MATCH_MP_TAC list_INDUCT);;
e (REWRITE_TAC[TMVARS_LIST; EX; NOT_IN_EMPTY]);;
e (INTRO_TAC "![y] [ys]; ind");;
e (ASM_REWRITE_TAC[IN_UNION]);;
let IN_TMVARS_LIST = top_thm();;
*)

let IN_TMVARS_LIST = prove
 (`!a xs. a IN TMVARS_LIST xs <=> EX (\x. a IN TMVARS x) xs`,
  GEN_TAC THEN MATCH_MP_TAC list_INDUCT THEN
  REWRITE_TAC[TMVARS_LIST; EX; NOT_IN_EMPTY] THEN
  INTRO_TAC "![y] [ys]; ind" THEN
  ASM_REWRITE_TAC[IN_UNION]);;

(* ------------------------------------------------------------------------- *)
(* Sostituzione per i termini.                                               *)
(* ------------------------------------------------------------------------- *)

(* ------------------------------------------------------------------------- *)
(* `TMSUBST f x` sostituisce dentro il termine x usando la mappa             *)
(* `f:num -> tm` da pensare come assegnazione di sostituzione a variabile    *)
(* numero `n` associo il termine `f n`.                                      *)
(* ------------------------------------------------------------------------- *)

let TMSUBST = new_recursive_definition tm_RECUR
  `(!f a. TMSUBST f (Var a) = f a) /\
   (!f k. TMSUBST f (Const k) = Const k)`;;

g `(!x. TMSUBST Var x = x)`;;
e (MATCH_MP_TAC tm_INDUCT);;
e (REWRITE_TAC[TMSUBST]);;
let TMSUBST_VAR = top_thm();;

g `(!f x g. TMSUBST f (TMSUBST g x) = TMSUBST (TMSUBST f o g) x)`;;
e (GEN_TAC THEN MATCH_MP_TAC tm_INDUCT);;
e (REWRITE_TAC[TMSUBST; o_THM]);;
let TMSUBST_TMSUBST = top_thm();;

g `!f x. (!a. a IN TMVARS x ==> f a = Var a) ==> TMSUBST f x = x`;;
e (GEN_TAC THEN MATCH_MP_TAC tm_INDUCT);;
e (REWRITE_TAC [TMVARS; TMSUBST; FORALL_IN_INSERT; NOT_IN_EMPTY]);;
let TMSUBST_ID = top_thm();;

g `(!f x. TMVARS x = {} ==> TMSUBST f x = x)`;;
e (GEN_TAC THEN MATCH_MP_TAC tm_INDUCT);;
e (REWRITE_TAC[TMVARS; TMSUBST; NOT_INSERT_EMPTY]);;
let CLOSED_IMP_TMSUBST_ID = top_thm();;

(* ------------------------------------------------------------------------- *)
(* Formulas.                                                                 *)
(* ------------------------------------------------------------------------- *)

parse_as_infix("&&",(16,"right"));;
parse_as_infix("||",(15,"right"));;
parse_as_infix("-->",(14,"right"));;
parse_as_infix("<->",(13,"right"));;
parse_as_prefix "Not";;

let quant_INDUCT,quant_RECUR = define_type
  "quant = Forall | Exists";;

let form_INDUCT,form_RECUR = define_type
  "form = Atom num (tm list)
        | Ge tm tm
        | Not form
        | && form form
        | || form form
	| --> form form
        | Quant quant num form";;

(* ------------------------------------------------------------------------- *)
(* Variabili dei termini.                                                    *)
(* ------------------------------------------------------------------------- *)
parse_term "False: form";;
let false_form  = new_definition `False = (p && Not p)`;; (*...*)

(* ------------------------------------------------------------------------- *)
(* Variabili libere.                                                         *)
(* ------------------------------------------------------------------------- *)

let IN_FVARS_RULES,IN_FVARS_INDUCT,IN_FVARS_CASES = new_inductive_set
  `(!a k xs. a IN TMVARS_LIST xs ==> a IN FVARS (Atom k xs)) /\
   (!a x1 x2. a IN TMVARS x1 \/ a IN TMVARS x2 ==> a IN FVARS (Ge x1 x2)) /\
   (!a p. a IN FVARS p ==> a IN FVARS (Not p)) /\
   (!a p q. a IN FVARS p ==> a IN FVARS (p && q)) /\
   (!a p q. a IN FVARS q ==> a IN FVARS (p && q)) /\
   (!a p q. a IN FVARS p ==> a IN FVARS (p || q)) /\
   (!a p q. a IN FVARS q ==> a IN FVARS (p || q)) /\
   (!a p q. a IN FVARS p ==> a IN FVARS (p --> q)) /\
   (!a p q. a IN FVARS q ==> a IN FVARS (p --> q)) /\
   (!u d i p. (SUC i,d) IN FVARS p ==> (i,d) IN FVARS (Quant u d p)) /\
   (!u d i e p. (i,d) IN FVARS p /\ ~(d = e) ==> (i,d) IN FVARS (Quant u e p))`;;

(*
let FVARS = prove
  (`(!k xs. FVARS (Atom k xs) = TMVARS_LIST xs) /\
   FVARS False = {} /\
   (!p q. FVARS (p --> q) = FVARS p UNION FVARS q) /\
   (!e p. FVARS (Forall e p) = {i,e | i | SUC i,e IN FVARS p} UNION
                               {i,d | ~(d = e) /\ i,d IN FVARS p})`,
  REPEAT STRIP_TAC THEN GEN_REWRITE_TAC I [EXTENSION] THEN GEN_TAC THEN
  GEN_REWRITE_TAC LAND_CONV [IN_FVARS_CASES] THEN
  REWRITE_TAC[distinctness "form"; injectivity "form";
              IN_TMVARS_LIST; NOT_IN_EMPTY] THENL
  [MESON_TAC[EX_MEM]; SET_TAC[]; SET_TAC[]]);;
*)

g `(!k xs. FVARS (Atom k xs) = TMVARS_LIST xs) /\
   (!x1 x2. FVARS (Ge x1 x2) = TMVARS x1 UNION TMVARS x2) /\
   (!p. FVARS (Not p) = FVARS p) /\
   (!p q. FVARS (p && q) = FVARS p UNION FVARS q) /\
   (!p q. FVARS (p || q) = FVARS p UNION FVARS q) /\
   (!p q. FVARS (p --> q) = FVARS p UNION FVARS q) /\
   (!u e p. FVARS (Quant u e p) = {i,e | i | SUC i,e IN FVARS p} UNION
                               {i,d | ~(d = e) /\ i,d IN FVARS p})`;;
e (REPEAT STRIP_TAC);;
e (GEN_REWRITE_TAC I [EXTENSION]);;
e (GEN_TAC);;
e (GEN_REWRITE_TAC LAND_CONV [IN_FVARS_CASES]);;
e (REWRITE_TAC [distinctness "form"; injectivity "form";
                IN_TMVARS_LIST; NOT_IN_EMPTY]);;
e (MESON_TAC [EX_MEM]);;
e (GEN_REWRITE_TAC I [EXTENSION]);;
e (GEN_TAC);;
e (GEN_REWRITE_TAC LAND_CONV [IN_FVARS_CASES]);;
e (REWRITE_TAC [distinctness "form"; injectivity "form"; TMVARS; IN_UNION]);;
e (MESON_TAC[]);;
e (GEN_REWRITE_TAC I [EXTENSION]);;
e (GEN_TAC);;
e (GEN_REWRITE_TAC LAND_CONV [IN_FVARS_CASES]);;
e (REWRITE_TAC[distinctness "form"; injectivity "form"]);;
e (MESON_TAC[]);;
e (GEN_REWRITE_TAC I [EXTENSION]);;
e (GEN_TAC);;
e (GEN_REWRITE_TAC LAND_CONV [IN_FVARS_CASES]);;
e (REWRITE_TAC[distinctness "form"; injectivity "form"; IN_UNION]);;
e (MESON_TAC[]);;
e (GEN_REWRITE_TAC I [EXTENSION]);;
e (GEN_TAC);;
e (GEN_REWRITE_TAC LAND_CONV [IN_FVARS_CASES]);;
e (REWRITE_TAC[distinctness "form"; injectivity "form"; IN_UNION]);;
e (MESON_TAC[]);;
e (GEN_REWRITE_TAC I [EXTENSION]);;
e (GEN_TAC);;
e (GEN_REWRITE_TAC LAND_CONV [IN_FVARS_CASES]);;
e (REWRITE_TAC[distinctness "form"; injectivity "form"; IN_UNION]);;
e (MESON_TAC[]);;
e (GEN_REWRITE_TAC I [EXTENSION]);;
e (GEN_TAC);;
e (GEN_REWRITE_TAC LAND_CONV [IN_FVARS_CASES]);;
e (REWRITE_TAC [distinctness "form"; injectivity "form"]);;
e (SET_TAC []);;
let FVARS = top_thm();;

(* ------------------------------------------------------------------------- *)
(* Sostituzione nelle formule                                                *)
(* ------------------------------------------------------------------------- *)

let TMBUMP = new_definition
  `TMBUMP e = TMSUBST (\ (i,d). Var((if d = e then SUC i else i),d))`;;

let SLIDE = new_definition
  `SLIDE e f (i,d) = if d = e
                     then if i = 0 
                          then Var(0,d)
                          else TMBUMP e (f(PRE i,d))
                     else f(i,d)`;;

let SUBST = define
  `(!f k xs. SUBST f (Atom k xs) = Atom k (MAP (TMSUBST f) xs)) /\
   (!f x1 x2. SUBST f (Ge x1 x2) = Ge (TMSUBST f x1) (TMSUBST f x2)) /\
   (!f p. SUBST f (Not p) = Not SUBST f p) /\
   (!f p q. SUBST f (p && q) = SUBST f p && SUBST f q) /\
   (!f p q. SUBST f (p || q) = SUBST f p || SUBST f q) /\
   (!f p q. SUBST f (p --> q) = SUBST f p --> SUBST f q) /\
   (!f u d p. SUBST f (Quant u d p) = Quant u d (SUBST (SLIDE d f) p))`;;

(* ek associa ad ogni nome di costante (un num) il nome del dominio (un num)   *)
(* ed associa ad ogni nome di dominio (un num) il dominio (un insieme num->bool*)
(* DOMAIN associa ad ogni termine il nome del dominio a cui appartiene.        *)

let DOMAIN = new_definition
  `DOMAIN ek d = {Const k | k | ek k = d}`;;

let KIND = define
  `(!ek i d. KIND ek (Var (i,d)) = d) /\
   (!ek k. KIND ek (Const k) = ek k)`;;

g `!ek f x. (!j e. KIND ek (f(j,e)) = e) ==> KIND ek (TMSUBST f x) = KIND ek x`;;
e (GEN_TAC THEN GEN_TAC THEN MATCH_MP_TAC tm_INDUCT);;
e (REWRITE_TAC[TMSUBST; FORALL_PAIR_THM; KIND]);;
e (MESON_TAC[]);;
let KIND_TMSUBST = top_thm();;

g `!ek e x. KIND ek (TMBUMP e x) = KIND ek x`;;
e (REPEAT GEN_TAC THEN REWRITE_TAC[TMBUMP]);;
e (MATCH_MP_TAC KIND_TMSUBST);;
e (REWRITE_TAC[KIND]);;
let KIND_TMBUMP = top_thm();;

g `!ek f i d. (!j e. KIND ek (f(j,e)) = e) ==> KIND ek (SLIDE e f (i,d)) = d`;;
e (REPEAT GEN_TAC THEN DISCH_TAC);;
e (REWRITE_TAC[SLIDE]);;
e COND_CASES_TAC;;
e (POP_ASSUM SUBST1_TAC);;
e COND_CASES_TAC;;
e (POP_ASSUM SUBST1_TAC);;
e (REWRITE_TAC[KIND]);;
e (REWRITE_TAC[KIND_TMBUMP]);;
e (ASM_REWRITE_TAC[]);;
e (ASM_REWRITE_TAC[KIND]);;
let KIND_SLIDE = top_thm();;

g `!ek a f g.
      (!i d. KIND ek (f(i,d)) = d) /\ (!i d. KIND ek (g(i,d)) = d)
      ==> SUBST f (SUBST g a) = SUBST (TMSUBST f o g) a`;;
e GEN_TAC;;
e (MATCH_MP_TAC form_INDUCT);;
e (REWRITE_TAC[SUBST; injectivity "form"]);;
e (REWRITE_TAC[GSYM MAP_o]);;
e (REWRITE_TAC[o_DEF; TMSUBST_TMSUBST; ETA_AX]);;
e (CONJ_TAC);;
e (REPEAT STRIP_TAC THEN ASM_SIMP_TAC[]);;
e (CONJ_TAC);;
e (REPEAT STRIP_TAC THEN ASM_SIMP_TAC[]);;
e (CONJ_TAC);;
e (REPEAT STRIP_TAC THEN ASM_SIMP_TAC[]);;
e (INTRO_TAC "![e] [p]; p; !f g; f g");;
e (ASM_SIMP_TAC[KIND_SLIDE]);;       
e (AP_THM_TAC THEN AP_TERM_TAC);;    
e (REWRITE_TAC[FUN_EQ_THM; FORALL_PAIR_THM]);;                       
e (INTRO_TAC "![i] [d]");;     
e (REWRITE_TAC[SLIDE]);;      
e (COND_CASES_TAC THEN REWRITE_TAC[]);;
e (POP_ASSUM (SUBST_ALL_TAC));;
e (COND_CASES_TAC THEN REWRITE_TAC[TMSUBST; SLIDE]);;    
e (REWRITE_TAC[TMBUMP]);;
e (REMOVE_THEN "g" (MP_TAC o SPECL[`PRE i`;`e:num`]));;
e (STRUCT_CASES_TAC (SPEC `g(PRE i:num,e:num):tm` (cases "tm")));;
e (STRUCT_CASES_TAC (ISPEC `a:num#num` PAIR_SURJECTIVE));;
e (REWRITE_TAC[KIND; TMSUBST]);;
e (DISCH_THEN SUBST1_TAC);;
e (REWRITE_TAC[TMSUBST; o_DEF; SLIDE; NOT_SUC; PRE]);;
e (REWRITE_TAC[KIND; TMBUMP]);;
e (REWRITE_TAC[KIND]);;
e (DISCH_THEN SUBST_VAR_TAC);;
e (REWRITE_TAC[TMSUBST; o_DEF; SLIDE; NOT_SUC; PRE]);;
e (REMOVE_THEN "g" (MP_TAC o SPECL[`i:num`;`d:num`]));;
e (STRUCT_CASES_TAC (SPEC `g(i:num,d:num):tm` (cases "tm")));;
e (REWRITE_TAC[TMSUBST]);;
e (STRUCT_CASES_TAC (ISPEC `a:num#num` PAIR_SURJECTIVE));;
e (REWRITE_TAC[KIND; SLIDE; injectivity "tm"; PAIR_EQ]);;
e (DISCH_THEN SUBST1_TAC);;
e (ASM_REWRITE_TAC[]);;
e (REWRITE_TAC[TMSUBST; o_DEF; SLIDE; NOT_SUC; PRE]);;
let SUBST_SUBST = top_thm();;

g `!p f. (!a. a IN FVARS p ==> f a = Var a)
           ==> SUBST f p = p `;;
e (MATCH_MP_TAC form_INDUCT);;
e (REWRITE_TAC[FVARS; SUBST; injectivity "form"]);;
e CONJ_TAC;;
e (MATCH_MP_TAC list_INDUCT);;
e (REWRITE_TAC [MAP; IN_TMVARS_LIST; EX]);;
e (SIMP_TAC [TMSUBST_ID]);;
e (SIMP_TAC[FORALL_IN_UNION; TMSUBST_ID; IN_UNION]);;
e (REWRITE_TAC[FORALL_IN_GSPEC]);;
e (REPEAT STRIP_TAC);;
e (FIRST_X_ASSUM MATCH_MP_TAC);;
e (REWRITE_TAC [FORALL_PAIR_THM; SLIDE]);;
e (REPEAT STRIP_TAC);;
e (COND_CASES_TAC);;
e (POP_ASSUM SUBST_VAR_TAC);;
e (COND_CASES_TAC);;
e (ASM_REWRITE_TAC[]);;
e (REWRITE_TAC[TMBUMP]);;
e (ASM_SIMP_TAC[ARITH_RULE `~(p1 = 0) ==> SUC (PRE p1) = p1`]);;
e (REWRITE_TAC [TMSUBST]);;
e (ASM_SIMP_TAC[ARITH_RULE `~(p1 = 0) ==> SUC (PRE p1) = p1`]);;
e (FIRST_X_ASSUM MATCH_MP_TAC);;
e (ASM_REWRITE_TAC[]);;
let SUBST_ID = top_thm();;

g `!f p. FVARS p = {} ==> SUBST f p = p`;;
e (REPEAT STRIP_TAC);;
e (MATCH_MP_TAC SUBST_ID);;
e (ASM_SIMP_TAC[NOT_IN_EMPTY]);;
let CLOSED_IMP_SUBST_ID = top_thm();;

(* ------------------------------------------------------------------------- *)
(* Prenex normal form.                                                       *)
(* ------------------------------------------------------------------------- *)

let matrix_RULES, matrix_INDUCT, matrix_CASES = new_inductive_definition
   `(!k xs. IS_MATRIX(Atom k xs)) /\
    (!x y. IS_MATRIX(Ge x y)) /\
    (!p. IS_MATRIX(p) ==> IS_MATRIX (Not p)) /\
    (!p q. IS_MATRIX(p) /\ IS_MATRIX(q) ==> IS_MATRIX(p && q)) /\
    (!p q. IS_MATRIX(p) /\ IS_MATRIX(q) ==> IS_MATRIX(p || q)) /\
    (!p q. IS_MATRIX(p) /\ IS_MATRIX(q) ==> IS_MATRIX(p --> q))`;;

let [IS_MATRIX_ATOM; IS_MATRIX_GE; IS_MATRIX_NOT; IS_MATRIX_AND;
     IS_MATRIX_OR; IS_MATRIX_IMP] = CONJUNCTS matrix_RULES;;

let prenex_RULES, prenex_INDUCT, prenex_CASES = new_inductive_definition
   `(!p. IS_MATRIX(p) ==> IS_PRENEX(p)) /\
    (!u e p. IS_PRENEX(p) ==> IS_PRENEX(Quant u e p))`;;

let [IS_PRENEX_MATRIX; IS_PRENEX_QUANT] = CONJUNCTS prenex_RULES;;

let QUANT_INVERT = new_recursive_definition quant_RECUR
  `QUANT_INVERT Forall = Exists /\
   QUANT_INVERT Exists = Forall`;;

let NNFCONV = define
  `(!neg k xs. NNFCONV neg (Atom k xs) =
               if neg then Atom k xs else Not(Atom k xs)) /\
   (!neg x y. NNFCONV neg (Ge x y) =
              if neg then Ge x y else Not(Ge x y)) /\
   (!neg p. NNFCONV neg (Not p) = NNFCONV (~neg) p) /\
   (!neg p q. NNFCONV neg (p && q) =
              (if neg
              then NNFCONV T p && NNFCONV T q 
	      else NNFCONV F p || NNFCONV F q)) /\
   (!neg p q. NNFCONV neg (p || q) =
              (if neg
              then NNFCONV T p || NNFCONV T q 
	      else NNFCONV F p && NNFCONV F q)) /\
   (!neg p q. NNFCONV neg (p --> q) =
              if neg
 	      then NNFCONV F p || NNFCONV T q
              else NNFCONV T p && NNFCONV F q) /\
   (!neg u e p. NNFCONV neg (Quant u e p) =
                if neg
 		then Quant u e (NNFCONV T p)
		else Quant (QUANT_INVERT u) e (NNFCONV F p))`;;

let BUMP = new_definition
  `BUMP e = SUBST (\ (i,d). Var ((if d = e then SUC i else i),d))`;;

(* Vantaggio: Si potrebbe dimostrare FORM_SIZE p = 0 <=> IS_MATRIX p *)
(* Ripassare le dimostrazioni con il FORM_SIZE corretto. -> PROBLEMA *)

let FORM_SIZE = new_recursive_definition form_RECUR
  `(!k xs. FORM_SIZE (Atom k xs) = 0) /\
   (!x y. FORM_SIZE (Ge x y) = 0) /\
   (!p. FORM_SIZE (Not p) = FORM_SIZE p + 1) /\
   (!p q. FORM_SIZE (p && q) = FORM_SIZE p + FORM_SIZE q + 1) /\
   (!p q. FORM_SIZE (p || q) = FORM_SIZE p + FORM_SIZE q + 1) /\
   (!p q. FORM_SIZE (p --> q) = FORM_SIZE p + FORM_SIZE q + 1) /\
   (!u e p. FORM_SIZE (Quant u e p) = FORM_SIZE p + 1)`;;

g `!p f. FORM_SIZE (SUBST f p) = FORM_SIZE p`;;
e (MATCH_MP_TAC form_INDUCT);;
e (REWRITE_TAC[FORM_SIZE; SUBST]);;
e (SIMP_TAC[]);;
let FORM_SIZE_SUBST = top_thm();;

g `!e p. FORM_SIZE (BUMP e p) = FORM_SIZE p`;;
e (REWRITE_TAC[BUMP; FORM_SIZE_SUBST]);;
let FORM_SIZE_BUMP = top_thm();;

let atom_RULES, atom_INDUCT, atom_CASES = new_inductive_definition
   `(!k xs. IS_ATOM (Atom k xs)) /\
    (!x y. IS_ATOM (Ge x y))`;;

let NNF_SIZE = new_recursive_definition form_RECUR
   `(!k xs. NNF_SIZE (Atom k xs) = 0) /\
    (!x y. NNF_SIZE (Ge x y) = 0) /\
    (!p q. NNF_SIZE (p && q) = NNF_SIZE p + NNF_SIZE q + 1) /\
    (!p q. NNF_SIZE (p || q) = NNF_SIZE p + NNF_SIZE q + 1) /\
    (!p q. NNF_SIZE (p --> q) = NNF_SIZE p + NNF_SIZE q + 1) /\
    (!u e p. NNF_SIZE (Quant u e p) = NNF_SIZE p + 1) /\
    (!p. NNF_SIZE (Not p) = if IS_ATOM(p) then 0
                            else NNF_SIZE p + 1)`;;

g `!p. NNF_SIZE p <= FORM_SIZE p`;;
e (MATCH_MP_TAC form_INDUCT);;
e (REWRITE_TAC[injectivity "form"; NNF_SIZE; FORM_SIZE; LE_0]);;
e (ARITH_TAC);;
let NNFSIZE_GE_FORMSIZE = top_thm();;

(*
e (CONJ_TAC);;

e (GEN_TAC);;
e (STRUCT_CASES_TAC (SPEC `a:form` (cases "form")));;
e (REWRITE_TAC[FORM_SIZE; NNF_SIZE; atom_RULES]);;
e (ARITH_TAC);;
e (STRUCT_CASES_TAC (SPEC `a:form` (cases "form")));;
e (REWRITE_TAC[FORM_SIZE; NNF_SIZE; atom_RULES]);;
e (ARITH_TAC);;

e (REWRITE_TAC[atom_CASES; distinctness "form"; NNF_SIZE; FORM_SIZE]);;
e (COND_CASES_TAC);;
e (ASM_REWRITE_TAC[]);;
e ARITH_TAC;;
e ARITH_TAC;;
e (REWRITE_TAC[atom_CASES; distinctness "form"; NNF_SIZE; FORM_SIZE]);;
e ARITH_TAC;;
e (REWRITE_TAC[atom_CASES; distinctness "form"; NNF_SIZE; FORM_SIZE]);;
e ARITH_TAC;;
e (REWRITE_TAC[atom_CASES; distinctness "form"; NNF_SIZE; FORM_SIZE]);;
e ARITH_TAC;;
e (REWRITE_TAC[atom_CASES; distinctness "form"; NNF_SIZE; FORM_SIZE]);;
e ARITH_TAC;;
e (ARITH_TAC);;


e (REPEAT STRIP_TAC);;
e (COND_CASES_TAC);;
e (ARITH_TAC);;
e (ARITH_TAC);;
e CONJ_TAC;;
e (REPEAT GEN_TAC THEN STRUCT_CASES_TAC (SPEC `(NNF_SIZE a0):num` (cases "num"))
   THEN STRUCT_CASES_TAC (SPEC `(FORM_SIZE a1):num` (cases "num")) THEN
   ARITH_TAC THEN ARITH_TAC);;
e CONJ_TAC;;
e (REPEAT GEN_TAC THEN STRUCT_CASES_TAC (SPEC `(NNF_SIZE a0):num` (cases "num"))
   THEN STRUCT_CASES_TAC (SPEC `(FORM_SIZE a1):num` (cases "num")) THEN
   ARITH_TAC THEN ARITH_TAC);;
e CONJ_TAC;;
e (REPEAT GEN_TAC THEN STRUCT_CASES_TAC (SPEC `(NNF_SIZE a0):num` (cases "num"))
   THEN STRUCT_CASES_TAC (SPEC `(FORM_SIZE a1):num` (cases "num")) THEN
   ARITH_TAC THEN ARITH_TAC);;
e (GEN_TAC THEN ARITH_TAC);;
let NNFSIZE_GE_FORMSIZE = top_thm();;
*)

g `!p. (NNF_SIZE (Not p) < FORM_SIZE (Not p))`;;
e (MATCH_MP_TAC form_INDUCT);;
e (REWRITE_TAC[FORM_SIZE; NNF_SIZE]);;
e (CONJ_TAC);;
e (REPEAT STRIP_TAC THEN REWRITE_TAC[atom_RULES] THEN ARITH_TAC);;
e (CONJ_TAC);;
e (REPEAT STRIP_TAC THEN REWRITE_TAC[atom_RULES] THEN ARITH_TAC);;
e CONJ_TAC;;

(* da rifare  *)
e (REPEAT STRIP_TAC THEN REWRITE_TAC[atom_CASES; distinctness "form"]
   THEN ARITH_TAC);;
e (CONJ_TAC);; 
e (REPEAT STRIP_TAC THEN REWRITE_TAC[atom_CASES; distinctness "form"]
   THEN ARITH_TAC);;
e (CONJ_TAC);; 
e (REPEAT STRIP_TAC THEN REWRITE_TAC[atom_CASES; distinctness "form"]
   THEN ARITH_TAC);;
e (CONJ_TAC);; 
e (REPEAT STRIP_TAC THEN REWRITE_TAC[atom_CASES; distinctness "form"]
   THEN ARITH_TAC);;
e (CONJ_TAC);; 
e (REPEAT STRIP_TAC THEN REWRITE_TAC[atom_CASES; distinctness "form"]
   THEN ARITH_TAC);;
e (REPEAT STRIP_TAC THEN REWRITE_TAC[atom_CASES; distinctness "form"]
   THEN ARITH_TAC);;
let NNFSIZE_G_FORMSIZE = top_thm();;
  
g `!p f. NNF_SIZE (SUBST f p) = NNF_SIZE p`;;
(* ... *)
let NNF_SIZE_SUBST = top_thm();;

g `!e p. NNF_SIZE (BUMP e p) = NNF_SIZE p`;;
e (REWRITE_TAC[BUMP; NNF_SIZE_SUBST]);;
let FORM_SIZE_BUMP = top_thm();;

g `!P. (!p. (!q. NNF_SIZE q < NNF_SIZE p ==> P q) ==> P p) ==> (!p. P p)`;;
e (INTRO_TAC "!P; wf");;
e (SUBGOAL_THEN `!n p. NNF_SIZE p = n ==> P p` (fun th -> MESON_TAC[th]));;
e (MATCH_MP_TAC num_WF);;
e (ASM_MESON_TAC[]);;

(* Oppure
e (INTRO_TAC "!n; ind; !p");;
e (HYP_TAC "ind" (REWRITE_RULE[RIGHT_IMP_FORALL_THM; IMP_IMP]));;
e (POP_ASSUM MP_TAC);;
e (SUBGOAL_THEN `(!m p. m < n /\ NNF_SIZE p = m ==> P p) <=>
                 (!p. NNF_SIZE p < n ==> P p)` SUBST1_TAC);;
e (MESON_TAC[]);;
e (INTRO_TAC "ind");;
e (DISCH_THEN SUBST_VAR_TAC);;
e (REMOVE_THEN "wf" MATCH_MP_TAC);;
e (ASM_REWRITE_TAC[]);;
*)

let NNF_FORM_INDUCT = top_thm();;

g `!p b. IS_MATRIX p ==> IS_MATRIX (NNFCONV b p)`;;
e (REWRITE_TAC[RIGHT_FORALL_IMP_THM]);;
e (MATCH_MP_TAC matrix_INDUCT);;
e (REWRITE_TAC[NNFCONV]);;
e (REPEAT STRIP_TAC THEN TRY COND_CASES_TAC THEN ASM_SIMP_TAC[matrix_RULES]);;
let IS_MATRIX_NNFCONV = top_thm();;


REWRITE_CONV[NNFCONV]
`NNFCONV T (Atom 0 [Var (0,1); Var (1,1)] --> Atom 1 [Var (0,2); Var (1,2)])`;;

`(Atom 0 [Var (0,1); Var (1,2)] || Quant Exists 2 (Atom 1 [Var (0,1); Var (1,1)])
   && (Atom 3 [Const 1; Const 2] && Quant Forall 1 
                                     (Atom 2 [Var (2,2); Var (3,1)])))`;;

`(Atom 0 [Var (0,1); Var (1,1)] --> Not (Quant Exists 1
                                        (Atom 1 [Var (0,1); Var (1,1)]))) ||
 Not ((Quant Forall 2 (Atom 2 [Var (2,2); Const 1])) && Atom 3 [Const 2])`;;

(* ------------------------------------------------------------------------- *)
(* Interpretazione dei termini.                                              *)
(* ------------------------------------------------------------------------- *)

let TMINTERP = define
  `(!ev a. TMINTERP ev (Var(a)) = ev a) /\
   (!ev k. TMINTERP ev (Const k) = k)`;;

let TMLIST_INTERP = define
  `(!ev. TMLIST_INTERP ev [] = []) /\
   (!ev x xs. TMLIST_INTERP ev (CONS x xs) = 
                 CONS (TMINTERP ev x) (TMLIST_INTERP ev xs))`;;

g `!ev f x. TMINTERP ev (TMSUBST f x) = TMINTERP (TMINTERP ev o f) x`;;
e (GEN_TAC THEN GEN_TAC);;
e (MATCH_MP_TAC tm_INDUCT);;	
e (REWRITE_TAC [TMSUBST; injectivity "tm"; TMINTERP; o_THM]);;
let TMINTERP_TMSUBST = top_thm();;

(* ------------------------------------------------------------------------- *)
(* Interpretazione delle formule                                             *)
(* ------------------------------------------------------------------------- *)

(*
let QUANT_INTERP = new_recursive_definition quant_RECUR
 `(QUANT_INTERP Forall = ((!):(num->bool) -> bool)) /\
  (QUANT_INTERP Exists = ((?):(num->bool) -> bool))`;;
*)

let IS_FORALL = new_recursive_definition quant_RECUR
  `IS_FORALL Forall = T /\
   IS_FORALL Exists = F`;;

let INTERP = define
  `(!ed ep ev k xs. INTERP ed ep ev (Atom k xs)
                <=> ep k (MAP (TMINTERP ev) xs)) /\
   (!ed ep ev x1 x2. INTERP ed ep ev (Ge x1 x2)
                <=>  TMINTERP ev x1 <= TMINTERP ev x2) /\
   (!ed ep ev p. INTERP ed ep ev (Not p) <=> ~(INTERP ed ep ev p)) /\
   (!ed ep ev p q. INTERP ed ep ev (p && q) <=> 
                  ((INTERP ed ep ev p) /\ (INTERP ed ep ev q))) /\
   (!ed ep ev p q. INTERP ed ep ev (p || q) <=> 
                  ((INTERP ed ep ev p) \/ (INTERP ed ep ev q))) /\
   (!ed ep ev p q. INTERP ed ep ev (p --> q) <=>
                  (INTERP ed ep ev (p)) ==> (INTERP ed ep ev (q))) /\ 
   (!ed ep ev u d p. INTERP ed ep ev (Quant u d p) <=>
      if IS_FORALL u
      then (!x. x IN ed d
        	        ==> INTERP ed ep
                                   (\ (i,e). if e = d
 				             then if i = 0
 					          then x
						  else ev (PRE i,d)
 					     else ev (i,e))
                                   p)
      else (?x. x IN ed d
        	        ==> INTERP ed ep
                                   (\ (i,e). if e = d
 				             then if i = 0
 					          then x
						  else ev (PRE i,d)
 					     else ev (i,e))
                                   p))`;;

(* map dest_var o frees per vedere le variabili libere in un termine *)

g `!ed ep p ev f.
      (!i d. KIND ek (f(i,d)) = d)
      ==> INTERP ed ep ev (SUBST f p) =
          INTERP ed ep (TMINTERP ev o f) p`;;
e (GEN_TAC THEN GEN_TAC);;
e (MATCH_MP_TAC form_INDUCT);;
e (REWRITE_TAC [SUBST; injectivity "form"; INTERP]);;
e CONJ_TAC;;
e (INTRO_TAC "![k] [xs] ev f; f");;
e (AP_TERM_TAC);;
e (REWRITE_TAC[GSYM MAP_o]);;
e (AP_THM_TAC THEN AP_TERM_TAC);;
e (REWRITE_TAC [FUN_EQ_THM]);;
e (REWRITE_TAC[o_THM]);;
e (REWRITE_TAC[TMINTERP_TMSUBST]);;
e CONJ_TAC;;
e (REWRITE_TAC[TMINTERP; TMINTERP_TMSUBST]);;
e CONJ_TAC;;
e (REPEAT GEN_TAC THEN STRIP_TAC);;
e (ASM_SIMP_TAC[]);;
e CONJ_TAC;;
e (REPEAT GEN_TAC THEN STRIP_TAC);;
e (ASM_SIMP_TAC[]);;
e CONJ_TAC;;
e (REPEAT GEN_TAC THEN STRIP_TAC);;
e (ASM_SIMP_TAC[]);;
e CONJ_TAC;;
e (REPEAT GEN_TAC THEN STRIP_TAC);;
e (ASM_SIMP_TAC[]);;
e (INTRO_TAC "! [d] [p]");;
(* mia modifica del codice prova *)
e (INTRO_TAC "ind");;
e COND_CASES_TAC;;
e (ASM_REWRITE_TAC[]);;
e (INTRO_TAC "!ev f; f");;
b();;
e (ASM_SIMP_TAC[KIND_SLIDE]);;
b();;

e (INTRO_TAC "ind; !ev f; f");;
e (ASM_SIMP_TAC[KIND_SLIDE]);;
e (SUBGOAL_THEN
`!x. x IN ed d ==>
          (TMINTERP
           (\ (i,e). if e = d
                    then if i = 0 then x else ev (PRE i,d)
                    else ev (i,e)) o
           SLIDE d f)
	   =
          (\ (i,e). if e = d
                   then if i = 0 then x else (TMINTERP ev o f) (PRE i,d)
                   else (TMINTERP ev o f) (i,e))`
  (fun th -> MESON_TAC[th]));;
e (INTRO_TAC "!x; x");;
e (REWRITE_TAC[FUN_EQ_THM; FORALL_PAIR_THM]);;
e (INTRO_TAC "![j] [e]");;
e (REWRITE_TAC[o_THM; SLIDE]);;
e (COND_CASES_TAC);;
e (POP_ASSUM SUBST_VAR_TAC);;
e (REWRITE_TAC[]);;
e (COND_CASES_TAC);;
e (POP_ASSUM SUBST_VAR_TAC THEN REWRITE_TAC[]);; 
e (REWRITE_TAC[TMINTERP]);; 
e (REWRITE_TAC[TMBUMP]);;
e (REMOVE_THEN "f" (MP_TAC o SPECL[`PRE j`;`d:num`]));;
e (STRUCT_CASES_TAC (SPEC `f(PRE j:num,d:num):tm` (cases "tm")));;
e (STRUCT_CASES_TAC (ISPEC `a:num#num` PAIR_SURJECTIVE));;
e (REWRITE_TAC[KIND; TMINTERP; TMSUBST]);;
e (DISCH_THEN SUBST1_TAC);;
e (REWRITE_TAC[]);;
e (REWRITE_TAC[NOT_SUC; PRE]);;
e (ASM_REWRITE_TAC[KIND; TMSUBST; TMINTERP]);;
e (REMOVE_THEN "f" (MP_TAC o SPECL[`j:num`;`e:num`]));;
e (STRUCT_CASES_TAC (SPEC `f(j:num,e:num):tm` (cases "tm")));;
e (STRUCT_CASES_TAC (ISPEC `a:num#num` PAIR_SURJECTIVE));;
e (REWRITE_TAC[KIND]);;
e (DISCH_THEN SUBST1_TAC);;
e (REWRITE_TAC[TMINTERP; TMSUBST]);;
e (ASM_REWRITE_TAC[]);;
e (REWRITE_TAC[TMINTERP]);;
let INTERP_SUBST = top_thm();;

(*----------------------------------------------------------------------*)
(* NNFCONV rispetta la semantica.                                       *)
(*----------------------------------------------------------------------*)

g `!ed ep p ev b. INTERP ed ep ev (NNFCONV b p) =
                  INTERP ed ep ev (if b then p else Not p)`;;
e (GEN_TAC THEN GEN_TAC);;
e (MATCH_MP_TAC NNF_FORM_INDUCT);;
e GEN_TAC;;
e (STRUCT_CASES_TAC (SPEC `p:form` (cases "form")) THEN
   REWRITE_TAC[NNF_SIZE; NNFCONV; LT]);;
e (ASM_CASES_TAC `IS_ATOM a` THEN ASM_REWRITE_TAC[]);;
e (INTRO_TAC "_; !ev b");;
e (POP_ASSUM (STRUCT_CASES_TAC o REWRITE_RULE[atom_CASES]) THEN
   REWRITE_TAC[NNFCONV] THEN BOOL_CASES_TAC `b:bool` THEN
   REWRITE_TAC[INTERP]);;
e (DISCH_THEN (MP_TAC o SPEC `a:form`));;
e ANTS_TAC;;
e ARITH_TAC;; 
e (DISCH_THEN (fun th -> REWRITE_TAC[th]));;
e (GEN_TAC THEN REWRITE_TAC[FORALL_BOOL_THM; INTERP]);;
e (DISCH_THEN (fun th -> MP_TAC (SPEC `a0:form` th) THEN
                         MP_TAC (SPEC `a1:form` th)));;
e ANTS_TAC;;
e ARITH_TAC;;
e (INTRO_TAC "a0");;
e ANTS_TAC;;
e ARITH_TAC;;
e (INTRO_TAC "a1");;
e (GEN_TAC THEN ASM_REWRITE_TAC[FORALL_BOOL_THM; INTERP; DE_MORGAN_THM]);;

e (DISCH_THEN (fun th -> MP_TAC (SPEC `a0:form` th) THEN
                         MP_TAC (SPEC `a1:form` th)));;
e ANTS_TAC;;
e ARITH_TAC;;
e (INTRO_TAC "a0");;
e ANTS_TAC;;
e ARITH_TAC;;
e (INTRO_TAC "a1");;
e (GEN_TAC THEN ASM_REWRITE_TAC[FORALL_BOOL_THM; INTERP; DE_MORGAN_THM]);;

e (DISCH_THEN (fun th -> MP_TAC (SPEC `a0:form` th) THEN
                         MP_TAC (SPEC `a1:form` th)));;
e ANTS_TAC;;
e ARITH_TAC;;
e (INTRO_TAC "a0");;
e ANTS_TAC;;
e ARITH_TAC;;
e (INTRO_TAC "a1");;
e (GEN_TAC THEN ASM_REWRITE_TAC[FORALL_BOOL_THM; INTERP]);;
e (MESON_TAC[]);;
e (DISCH_THEN (MP_TAC o SPEC `a2:form`));;
e ANTS_TAC;;
e ARITH_TAC;;
e (INTRO_TAC "ind");;
e (GEN_TAC THEN ASM_REWRITE_TAC[FORALL_BOOL_THM]);;


e (CONJ_TAC);;
e (REWRITE_TAC[INTERP]);;
e (COND_CASES_TAC THEN ASM_REWRITE_TAC[]);;

e (STRUCT_CASES_TAC (SPEC `a0: quant` (cases "quant")));;
e (REWRITE_TAC[QUANT_INVERT; INTERP; IS_FORALL]);;
e (REWRITE_TAC[NOT_FORALL_THM]);;








e (SIMP_TAC[FORALL_NOT_THM]);;
(*----------------------------------------------------------------------*)
(* Query.                                                               *)
(*----------------------------------------------------------------------*)

let query_tybij = new_type_definition "query" ("mk_query","dest_query")
 (prove
    (`?(xs,p). TMVARS_LIST xs = FVARS p`,
     REWRITE_TAC[EXISTS_PAIRED_THM] THEN
     EXISTS_TAC `[]:tm list` THEN
     EXISTS_TAC `Atom k []` THEN
     REWRITE_TAC[TMVARS_LIST; FVARS]));;

let query_form = new_definition
  `query_form q = SND (dest_query q)`;;

let query_tms = new_definition
  `query_tms q = FST (dest_query q)`;;

let database = define_type
  "database = Database (num#num list -> bool)";;

let database_rels = define
  `database_rels (Database db) = db`;;

let EP_OF_DB = define
  `EP_OF_DB (Database db) k cs <=> db(k,cs)`;;  

let DBINTERP = new_definition
  `DBINTERP ed db ev p <=> INTERP ed (EP_OF_DB db) ev p`;;

let query_tpls = new_definition
  `query_tpls ed db q =
     {MAP (TMINTERP ev) (query_tms q) | ev |
      DBINTERP ed db ev (query_form q)}`;; 

let run_query = define
  `run_query ed q k db =
     Database ({k,cs | cs IN query_tpls ed db q} UNION
               {j,cs | j,cs IN database_rels /\ ~(j = k)} db)`;;


(*
let PNFSTEP2_CONV = new_recursive_definition form_RECUR
   `(!k xs. PNFSTEP2_CONV (Atom k xs) = Atom k xs) /\
    (!p. PNFSTEP2_CONV (Not p) =  PNFSTEP2 Not p)) /\
    (!p q. PNFSTEP2_CONV (p && q) = (PNFSTEP2_CONV p) && (PNFSTEP2_CONV q)) /\
    (!p q. PNFSTEP2_CONV (p || q) = (PNFSTEP2_CONV p) || (PNFSTEP2_CONV q)) /\
    (!u e p. PNFSTEP2_CONV (Quant u e p) = Quant u e  (PNFSTEP2_CONV p))`;;


let FMPRENEX_IMP' = define
  `(!e p q. FMPRENEX_IMP' (Forall e p) q = Exists e (FMPRENEX_IMP' p q)) /\
   (!e p q. FMPRENEX_IMP' p (Forall e q) = Forall e (FMPRENEX_IMP' p q)) /\
   (!q. FMPRENEX_IMP' False q = False --> q)`;;

let FMPRENEX_IMP1 = define
 `FMPRENEX_IMP1 p q =
      match p with
        Forall e r -> Exists e (FMPRENEX_IMP1 r q)
      | _ -> match q with
               Forall e r -> Forall e (FMPRENEX_IMP1 p r)
             | _ -> p --> q`;;


let FMPRENEX' = new_recursive_definition form_RECUR
  `(!k xs. FMPRENEX' (Atom k xs) = Atom k xs) /\
   FMPRENEX False = False /\
   (!p q. FMPRENEX' (p --> q) =
          FMPRENEX_IMP' (FMPRENEX' p) (FMPRENEX' q)) /\
   (!e p. FMPRENEX' (Forall e p) = Forall e (FMPRENEX' p))`;;

let FMPRENEX_IMP = define
  `FMPRENEX_IMP p =
     match p with
       (Forall e p) --> q -> Exists e (FMPRENEX_IMP (p --> q))
     | p --> (Forall e q) -> Forall e (FMPRENEX_IMP (p --> q))
     | _ -> p`;;

let FMPRENEX = new_recursive_definition form_RECUR
  `(!k xs. FMPRENEX (Atom k xs) = Atom k xs) /\
   FMPRENEX False = False /\
   (!p q. FMPRENEX (p --> q) =
          FMPRENEX_IMP (FMPRENEX p --> FMPRENEX q)) /\
   (!e p. FMPRENEX (Forall e p) = Forall e (FMPRENEX p))`;;

g `!ed ep ev p q.
     IS_PRENEX p /\ IS_PRENEX q
     ==> (INTERP ed ep ev (FMPRENEX_IMP p) <=> INTERP ed ep ev (p))`;;

g `!ed ep ev.
     !p. IS_PRENEX p
         ==> !q. IS_PRENEX q
                 ==> (INTERP ed ep ev (FMPRENEX_IMP p) <=>
                      INTERP ed ep ev (p))`;;

g `!ed ep ev p.INTERP ed ep ev (FMPRENEX_IMP p ) <=> INTERP ed ep ev (p)`;;
e (GEN_TAC THEN GEN_TAC THEN GEN_TAC);;
e (MATCH_MP_TAC form_INDUCT);;
e (CONJ_TAC);;
e (REWRITE_TAC[FMPRENEX_IMP]);;
e (CONJ_TAC);;
e (REWRITE_TAC[FMPRENEX_IMP]);;
e (CONJ_TAC);;
e (INTRO_TAC "![p] [q]; php qhp");;
e (ONCE_REWRITE_TAC[FMPRENEX_IMP]);;
e (REWRITE_TAC[INTERP]);;

MATCH_CONV 
`match p --> q with
    Forall e p --> q -> Exists e (FMPRENEX_IMP (p --> q))
  | p --> Forall e q -> Forall e (FMPRENEX_IMP (p --> q))
  | _ -> p --> q`;;

ONCE_DEPTH_CONV MATCH_CONV 
`INTERP ed ep ev
 (match p --> q with
    Forall e p --> q -> Exists e (FMPRENEX_IMP (p --> q))
  | p --> Forall e q -> Forall e (FMPRENEX_IMP (p --> q))
  | _ -> p --> q) <=>
 INTERP ed ep ev p ==> INTERP ed ep ev q`;;

e (CONV_TAC (ONCE_DEPTH_CONV MATCH_CONV));;
e COND_CASES_TAC;;
e (POP_ASSUM (DESTRUCT_TAC "@e r. r"));;
e (POP_ASSUM SUBST_VAR_TAC);;
e (REWRITE_TAC[]);;

e (STRUCT_CASES_TAC (SPEC `p:form` (cases "form")));;
e (STRUCT_CASES_TAC (SPEC `q:form` (cases "form")));;

e (STRIP_TAC);;
e (REWRITE_TAC[GSYM INTERP]);;
e (REWRITE_TAC[INTERP; FMPRENEX_IMP] THEN SIMP_TAC[]);;
e (REWRITE_TAC[INTERP; FMPRENEX_IMP] THEN SIMP_TAC[]);;
e (ASM_REWRITE_TAC[]);;
e (REWRITE_TAC[GSYM INTERP]);;
e (REWRITE_TAC[FMPRENEX_IMP]);;  (*VA Ad aprire la formula ad infinitum?*)


g `!ed ep ev p. INTERP ed ep ev (p) = INTERP ed ep ev (FMPRENEX p)`;;
e (GEN_TAC THEN GEN_TAC THEN GEN_TAC);;
e (MATCH_MP_TAC form_INDUCT);;
e (CONJ_TAC);;
e (INTRO_TAC "! [k] [xs]");;
e (REWRITE_TAC[INTERP; FMPRENEX]);;
e (CONJ_TAC);;
e (REWRITE_TAC[FMPRENEX]);;
e (CONJ_TAC);;
e (INTRO_TAC "! [p] [q]");;
e (REPEAT STRIP_TAC);;
e (REWRITE_TAC[INTERP; FMPRENEX]);;   (*poi devo dimostrare quello sopra*)



`query_tpls ed db q = query_tpls ed db (FMPRENEX q)`

`run_query ed q k db = run_query ed (FMPRENEX q) k db`;;
*)

(* 
(map dest_var o frees)
  `(!ed ep ev k xs. INTERP ed ep ev (Atom k xs)
                <=> ep k (TM_LIST_INTERP ev xs)) /\
   (!ed ep ev. INTERP ed ep ev False <=> F) /\
   (!ed ep ev p q. INTERP ed ep ev (p --> q) <=>
                   (INTERP ed ep ev (p)) ==> (INTERP ed ep ev (q))) /\ 
   (!ed ep ev d p. INTERP ed ep ev (Forall d p) <=>
                   (!x. x IN ed d
        	        ==> INTERP ed ep
                                   (\ (i,e). if e = d
 				             then if i = 0
 					          then x
						  else ev (PRE i,d)
					     else ev (i,e))
                                   p))`;;
*)
