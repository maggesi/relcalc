(* ========================================================================= *)
(* Prova di implementazione di un linguaggio del primo ordine.               *)
(* ========================================================================= *)

(* ------------------------------------------------------------------------- *)
(* Termini.                                                                  *)
(* ------------------------------------------------------------------------- *)

let tm_INDUCT,tm_RECUR = define_type
  "tm = Var num
      | Fn num (tm list)";;

(* ------------------------------------------------------------------------- *)
(* Variabili.                                                                *)
(* ------------------------------------------------------------------------- *)

let TMVARS = new_recursive_definition tm_RECUR
  `(!n. TMVARS (Var n) = {n}) /\
   (!k xs. TMVARS (Fn k xs) = TMVARS_LIST xs) /\
   (TMVARS_LIST [] = {}) /\
   (!x xs. TMVARS_LIST (CONS x xs) = TMVARS x UNION TMVARS_LIST xs)`;;

(*
let TMVARS = define
  `(!n. TMVARS (Var n) = {n}) /\
   (!k xs. TMVARS (Fn k xs) = UNIONS {TMVARS x | MEM x xs})`;;

let TMVARS = define
  `(!n. TMVARS (Var n) = {n}) /\
   (!k xs. TMVARS (Fn k xs) = ITLIST (UNION) (MAP FVARS xs) {})`;;

g `!f. TMVARS_LIST f = MAP (TMVARS f)`;;
let TMVARS_LIST = top_thm();;
*)

(* ------------------------------------------------------------------------- *)
(* `TMSUBST f x` sostituisce dentro il termine x usando la mappa             *)
(* `f:num -> tm` da pensare come assegnazione di sostituzione a variabile    *)
(* numero `n` associo il termine `f n`.                                      *)
(* ------------------------------------------------------------------------- *)

let TMSUBST = new_recursive_definition tm_RECUR
  `(!f n. TMSUBST f (Var n) = f n) /\
   (!f k xs. TMSUBST f (Fn k xs) = Fn k (TMSUBST_LIST f xs)) /\
   (!f. TMSUBST_LIST f [] = []) /\
   (!f x xs. TMSUBST_LIST f (CONS x xs) =
             CONS (TMSUBST f x) (TMSUBST_LIST f xs))`;;

g `(!x. TMSUBST Var x = x) /\
   (!xs. TMSUBST_LIST Var xs = xs)`;;
e (MATCH_MP_TAC tm_INDUCT);;
e (REWRITE_TAC[TMSUBST; injectivity "tm"]);;
let TMSUBST_VAR = top_thm();;

g `(!x f g. TMSUBST f (TMSUBST g x) = TMSUBST (TMSUBST f o g) x) /\
   (!xs f g. TMSUBST_LIST f (TMSUBST_LIST g xs) =
             TMSUBST_LIST (TMSUBST f o g) xs)`;;
e (MATCH_MP_TAC tm_INDUCT);;
e (REWRITE_TAC[TMSUBST; injectivity "tm"; o_THM]);;
e (SIMP_TAC[]);;
let TMSUBST_TMSUBST = top_thm();;

g `(!x f. (!n. n IN TMVARS x ==> f n = Var n) ==> TMSUBST f x = x) /\
   (!xs f. (!n. n IN TMVARS_LIST xs ==> f n = Var n)
                      ==> TMSUBST_LIST f xs = xs)`;;
e (MATCH_MP_TAC tm_INDUCT);;
e (REWRITE_TAC [TMVARS; TMSUBST]);;
e (REWRITE_TAC [injectivity "tm"]);;
e (REWRITE_TAC [FORALL_IN_INSERT; NOT_IN_EMPTY; FORALL_IN_UNION]);;
e (SIMP_TAC[]);;
let TMSUBST_ID = top_thm();;

g `(!x f. TMVARS x = {} ==> TMSUBST f x = x) /\
   (!xs f. TMVARS_LIST xs = {} ==> TMSUBST_LIST f xs = xs)`;;
e (MATCH_MP_TAC tm_INDUCT);;
e (REWRITE_TAC [TMVARS; TMSUBST]);;
e (REWRITE_TAC [injectivity "tm"]);;
e (REWRITE_TAC [NOT_INSERT_EMPTY; EMPTY_UNION]);;
e (SIMP_TAC[]);;
let CLOSED_IMP_TMSUBST_ID = top_thm();;

let BUMP = new_definition
  `BUMP = TMSUBST (Var o SUC)`;;

let SLIDE = define
  `(!f. SLIDE f 0 = Var 0) /\
   (!f n. SLIDE f (SUC n) = BUMP (f n))`;;

(* ------------------------------------------------------------------------- *)
(* Formule.                                                                  *)
(* ------------------------------------------------------------------------- *)

parse_as_infix("&&",(16,"right"));;
parse_as_infix("||",(15,"right"));;
parse_as_infix("-->",(14,"right"));;
parse_as_infix("<->",(13,"right"));;
parse_as_prefix "Not";;
parse_as_prefix "Forall";;
parse_as_prefix "Exists";;


let form_INDUCT,form_RECUR = define_type
  "form = Atom num (tm list)
        | False
        | --> form form
        | Forall form";;

(*let form_INDUCT,form_RECUR = define_type
  "form = Atom tm
        | False
        | --> form form
        | Forall form";;
*)

let form_not = new_definition
  `Not p = p --> False`;;

let form_or = new_definition
  `p || q = Not p --> q`;;

let form_and = new_definition
  `p && q = Not (p --> Not q)`;;

let form_iff = new_definition
  `p <-> q = (p --> q) && (q --> p)`;;

let form_exists = new_definition
  `Exists p = Not Forall Not p`;;

(* ------------------------------------------------------------------------- *)
(* Variabili libere.                                                         *)
(* ------------------------------------------------------------------------- *)

let FVARS = define
  `(!k tms. FVARS (Atom k tms) = TMVARS_LIST tms) /\
   FVARS False = {} /\
   (!p q. FVARS (p --> q) = FVARS p UNION FVARS q) /\
   (!p. FVARS (Forall p) = {n | SUC n IN FVARS p})`;;

(*
let FVARS = define
  `(!tm. FVARS (Atom tm) = TMVARS tm) /\
   FVARS False = {} /\
   (!p q. FVARS (p --> q) = FVARS p UNION FVARS q) /\
   (!p. FVARS (Forall p) = {n | SUC n IN FVARS p})`;;
*)

(*----------------------------------------*)
(* Prove on De Bruijn indexes.            *)
(*----------------------------------------*)

g `FVARS (Atom 0 [Var 0; Var 1; Var 2]) = [0; 1; 2]`;;
e (REWRITE_TAC [FVARS; TMVARS]);;
e (REWRITE_TAC [IN_UNION; IN_SING]);;
e (REWRITE_TAC [NOT_SUC]);;
e (REWRITE_TAC [ARITH_RULE `SUC n = 3 \/ SUC n = 1 <=> n = 2 \/ n = 0`]);;
e (SET_TAC []);;
top_thm();;

(* ------------------------------------------------------------------------- *)
(* Sostituzione.                                                             *)
(* ------------------------------------------------------------------------- *)

let SUBST = define
  `(!f k tms. SUBST f (Atom k tms) = Atom k (TMSUBST_LIST f tms)) /\
   (!f. SUBST f False = False) /\
   (!f p q. SUBST f (p --> q) = SUBST f p --> SUBST f q) /\
   (!f p. SUBST f (Forall p) = Forall (SUBST (SLIDE f) p))`;;

g `!a f g. SUBST f (SUBST g a) = SUBST (TMSUBST f o g) a`;;
e (MATCH_MP_TAC form_INDUCT);;
e (REPEAT STRIP_TAC THEN
   ASM_REWRITE_TAC[SUBST; o_THM; TMSUBST_TMSUBST; injectivity "form"]);;
e (AP_THM_TAC THEN AP_TERM_TAC);;
e (REWRITE_TAC[FUN_EQ_THM]);;
e (INTRO_TAC "![n]");;
e (REWRITE_TAC[o_THM; SLIDE]);;
e (STRUCT_CASES_TAC (SPEC `n:num` (cases "num")));;
e (REWRITE_TAC[SLIDE; TMSUBST]);;
e (REWRITE_TAC[o_THM; SLIDE; BUMP]);;
e (REWRITE_TAC[TMSUBST_TMSUBST]);;
e (AP_THM_TAC THEN AP_TERM_TAC);;
e (REWRITE_TAC[FUN_EQ_THM]);;
e (INTRO_TAC "![n]");;
e (REWRITE_TAC[o_THM; TMSUBST; SLIDE; BUMP]);;
let SUBST_SUBST = top_thm();;

g `!p f. (!n. n IN FVARS p ==> f n = Var n) ==> SUBST f p = p`;;
e (MATCH_MP_TAC form_INDUCT);;
e (REWRITE_TAC [FVARS; SUBST]);;
e (REWRITE_TAC [injectivity "form"]);;
e (SIMP_TAC [TMSUBST_ID]);;
e (REWRITE_TAC [IN_UNION] THEN SIMP_TAC []);;
e (REWRITE_TAC [FORALL_IN_GSPEC]);;
e (REPEAT STRIP_TAC);;
e (FIRST_X_ASSUM MATCH_MP_TAC);;
e GEN_TAC;;
e (STRUCT_CASES_TAC (SPEC `n:num` (cases "num"))THEN REWRITE_TAC [SLIDE]);;
e (REWRITE_TAC [BUMP]);;
e (DISCH_TAC);;
e (ASM_SIMP_TAC []);;
e (REWRITE_TAC [TMSUBST; o_THM]);;
let SUBST_ID = top_thm();;

g `!p f. FVARS p = {} ==> SUBST f p = p`;;
e (REPEAT STRIP_TAC);;
e (MATCH_MP_TAC SUBST_ID);;
e (ASM_REWRITE_TAC [NOT_IN_EMPTY]);;
let CLOSED_IMP_SUBST_ID = top_thm();;

(* ------------------------------------------------------------------------- *)
(* Provability.                                                              *)
(* ------------------------------------------------------------------------- *)

parse_as_infix("|--",(11,"right"));;

let provable_RULES,provable_INDUCT,provable_CASES = new_inductive_definition
  `(!p. p IN A ==> A |-- p) /\
   (!p q. A |-- p --> (q --> p)) /\
   (!p q r. A |-- (p --> q --> r) --> (p --> q) --> (p --> r)) /\
   (!p. A |-- ((p --> False) --> False) --> p) /\
   (!p q. A |-- p --> q /\ A |-- p ==> A |-- q) /\
   (!p t. A |-- Forall p
          ==> A |-- SUBST (\n. if n = 0 then t else Var (PRE n)) p) /\
   (!p q. A |-- Forall (p --> q) ==> A |-- Forall p --> Forall q) /\
   (!p v. ~(v IN FVARS p) /\ A |-- p
          ==> A |-- Forall
                      (SUBST (\n. if n < v then Var v else
                                  if v < n then Var 0 else
                                  Var (PRE v)) p))`;;

let [provable_IN; provable_AX1; provable_AX2; provable_AX3;
   provable_MP; provable_AX4; provable_AX5; provable_GEN] =
CONJUNCTS (REWRITE_RULE [FORALL_AND_THM] provable_RULES);;

let provable_DEDUCTION_THM = new_axiom
  `!A p q. FVARS p = {} /\ p INSERT A |-- q ==> A |-- p --> q`;;

g `!A p. A |-- p --> p`;;
e (MESON_TAC [provable_RULES]);;
let provable_REFL = top_thm();;

g `!A p q r. A |-- p --> q /\ A |-- q --> r ==> A |-- p --> r`;;
e (MESON_TAC [provable_RULES; provable_DEDUCTION_THM]);;
let provable_TRANSITIVE_THM = top_thm();;

g `! A p q r. A |-- p --> (q --> r) /\ A |-- q ==> A |-- p --> r`;;
e (MESON_TAC[provable_RULES; provable_DEDUCTION_THM]);;
let provable_WEAKENING_THM = top_thm();;

g `!A p. A |-- Not Not p --> p`;;
e (REPEAT GEN_TAC);;
e (REWRITE_TAC[form_not]);;
e (MATCH_ACCEPT_TAC provable_AX3);;
let provable_DOUB_NEG_CL = top_thm();;

g `!A p. A |-- p --> Not Not p`;;
e (REPEAT GEN_TAC);;
e (REWRITE_TAC[form_not]);;
e (MATCH_MP_TAC provable_TRANSITIVE_THM);;
e (EXISTS_TAC `Not Not Not p --> p`);;
e (REWRITE_TAC[form_not]);;
e CONJ_TAC;;
e (MATCH_ACCEPT_TAC provable_AX1);;
e (MESON_TAC [provable_RULES; provable_DOUB_NEG_CL]);;
let provable_DOUB_NEG_INT = top_thm();;

g `!A p q. A |-- Not p --> (p --> q)`;;
e (REPEAT GEN_TAC);;
e (REWRITE_TAC[form_not]);;
e (MESON_TAC [provable_RULES]);; 
let provable_EX_FALSO_SQ = top_thm();;

g `!A p q. A |-- (Not p--> Not q) --> ((Not p --> q) --> p)`;;
e (REWRITE_TAC [form_not]);;
e (GEN_TAC);;
e (MESON_TAC [provable_RULES]);;
let provable_CONV_AX3 = top_thm();;

g `!A p q. FVARS p = {} /\ FVARS q = {}
           ==> A |-- (Not q --> Not p) --> (p --> q)`;;
e (REPEAT STRIP_TAC THEN REWRITE_TAC[form_not]);;
e (MATCH_MP_TAC provable_DEDUCTION_THM);;
e CONJ_TAC;;
e (ASM_REWRITE_TAC [FVARS; UNION_EMPTY]);;
e (MATCH_MP_TAC provable_TRANSITIVE_THM);;
e (EXISTS_TAC `Not q --> p`);;
e (REWRITE_TAC[form_not]);;
e (CONJ_TAC);;
e (REWRITE_TAC [form_not]);;
e (MATCH_ACCEPT_TAC provable_AX1);;
e (MATCH_MP_TAC provable_MP);;
e (EXISTS_TAC `Not q --> Not p`);;
e (REWRITE_TAC[form_not]);;
e (CONJ_TAC);;
e (MATCH_ACCEPT_TAC (REWRITE_RULE[form_not] provable_CONV_AX3));;
e (MATCH_MP_TAC provable_IN);;
e (REWRITE_TAC[IN_INSERT]);;
let provable_STRONG_CONTR = top_thm();;

g `!A p q. A |-- (p --> q) --> (Not q --> Not p)`;;
e (REPEAT GEN_TAC);;
e (REWRITE_TAC [form_not]);;
e (MATCH_MP_TAC provable_DEDUCTION_THM);;
e (MATCH_MP_TAC provable_MP);;
e (EXISTS_TAC `Not Not p --> Not Not q`);;
e CONJ_TAC;;
e (REWRITE_TAC [form_not]);;
e (MATCH_ACCEPT_TAC (REWRITE_RULE [form_not] provable_STRONG_CONTR));;
e (MATCH_MP_TAC provable_TRANSITIVE_THM);;
e (EXISTS_TAC `q:form`);;
e (REWRITE_TAC [form_not]);;
e CONJ_TAC;;
e (MATCH_MP_TAC provable_TRANSITIVE_THM);;
e (EXISTS_TAC `p:form`);;
e CONJ_TAC;;
e (MATCH_ACCEPT_TAC (REWRITE_RULE[form_not] provable_DOUB_NEG_CL));;
IN_INSERT;;
e (MATCH_MP_TAC provable_IN);;
e (REWRITE_TAC [IN_INSERT]);;
e (MATCH_ACCEPT_TAC (REWRITE_RULE [form_not] provable_DOUB_NEG_INT));;
let provable_WEAK_CONTR = top_thm();;

g `!A p q. A |-- p --> (Not q --> Not (p --> q))`;;
e (REPEAT GEN_TAC);;
e (REWRITE_TAC [form_not]);;
e (MATCH_MP_TAC provable_TRANSITIVE_THM);;
e (EXISTS_TAC `(p -->q) --> q`);;
e CONJ_TAC;;
e (MATCH_MP_TAC provable_DEDUCTION_THM);;
e (MATCH_MP_TAC provable_DEDUCTION_THM);;
e (MATCH_MP_TAC provable_MP);;
e (EXISTS_TAC `p:form`);;
e CONJ_TAC;;
e (MATCH_MP_TAC provable_IN);;
e (REWRITE_TAC [IN_INSERT]);;
e (MATCH_ACCEPT_TAC (REWRITE_RULE [form_not] provable_WEAK_CONTR));;
let provable_TRUE_OF_IMP = top_thm();;


g `!A p q. A |-- (p --> q) --> ((Not p --> q) --> q)`;;
e (REWRITE_TAC [form_not]);;
e (REPEAT GEN_TAC);;
e (MATCH_MP_TAC provable_DEDUCTION_THM);;
e (MATCH_MP_TAC provable_DEDUCTION_THM);;
e (MATCH_MP_TAC provable_MP);;
e (EXISTS_TAC `Not q --> Not p`);;
e (CONJ_TAC);;
e (MATCH_MP_TAC provable_MP);;
e (EXISTS_TAC `Not q --> Not Not p`);;
e (CONJ_TAC);;
e (MESON_TAC [provable_RULES; form_not; provable_CONV_AX3]);;
e (MATCH_MP_TAC provable_MP);;
e (EXISTS_TAC `Not p --> q`);;
e (CONJ_TAC);;
e (MATCH_ACCEPT_TAC provable_WEAK_CONTR);;
e (MESON_TAC [IN_INSERT; provable_RULES; form_not;
   provable_DEDUCTION_THM]);;
e (MATCH_MP_TAC provable_MP);;
e (EXISTS_TAC `p --> q`);;
e (CONJ_TAC);;
e (MATCH_ACCEPT_TAC provable_WEAK_CONTR);;
e (MESON_TAC [IN_INSERT; provable_RULES; form_not;
   provable_DEDUCTION_THM]);;
let provable_EX_ABSURDIS_SF = top_thm();;

g `!A p q. A |-- p --> (p || q)`;;
e (REWRITE_TAC [form_or]);;
e (REWRITE_TAC [form_not]);;
e (REPEAT GEN_TAC);;
e (MATCH_MP_TAC provable_DEDUCTION_THM);;
e (MATCH_MP_TAC provable_DEDUCTION_THM);;
e (MATCH_MP_TAC provable_MP);;
e (EXISTS_TAC `p:form`);;
e (CONJ_TAC);;
e (MATCH_MP_TAC provable_MP);;
e (EXISTS_TAC `p --> False`);;
e (CONJ_TAC);;
e (MESON_TAC [provable_EX_FALSO_SQ; provable_RULES]);;
(*sempre lo stesso problema*)


g `!A p q. A |-- p && q --> p`;;
e (REPEAT GEN_TAC);;
e (REWRITE_TAC [form_and]);;
e (REWRITE_TAC [form_not]);;
e (MATCH_MP_TAC provable_TRANSITIVE_THM);;
e (EXISTS_TAC `Not Not p`);;
e (REWRITE_TAC [form_not]);;
e CONJ_TAC;;
e (MATCH_MP_TAC provable_MP);;
e (EXISTS_TAC `Not p --> (p --> Not q)`);;
e (REWRITE_TAC [form_not]);;
e CONJ_TAC;;
e (MATCH_ACCEPT_TAC (REWRITE_RULE [form_not] provable_WEAK_CONTR));;
e (MATCH_ACCEPT_TAC (REWRITE_RULE [form_not] provable_EX_FALSO_SQ));;
e (MATCH_ACCEPT_TAC (REWRITE_RULE [form_not] provable_DOUB_NEG_CL));;
let provable_SEMPLIFICATION_LAW1 = top_thm();;
